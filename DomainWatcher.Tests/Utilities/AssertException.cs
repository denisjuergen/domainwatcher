﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DomainWatcher.Tests.Utilities
{
    public static class AssertException
    {
        public static void IsThrown(Action action, Type exceptionType)
        {
            try
            {
                action();
            }
            catch (Exception ex)
            {
                Assert.IsInstanceOfType(ex, exceptionType, "The thrown exception isn't an instance of the expected type.");
                return;
            }
            Assert.Fail("No exception was thrown.");
        }

        public static void IsThrown<T>(Action action) where T : Exception
        {
            AssertException.IsThrown(action, typeof(T));
        }

        public static void IsThrown(Action action)
        {
            AssertException.IsThrown(action, typeof(Exception));
        }

        public static void IsNotThrown(Action action)
        {
            try
            {
                action();
            }
            catch
            {
                Assert.Fail("An exception was thrown.");
            }
        }
    }
}
