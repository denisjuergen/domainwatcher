﻿namespace DomainWatcher.Tests.Mocks
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using DomainWatcher.Architecture.Components;

    public class ValueHolder<T> : ObservableObject
    {
        private T value;

        public T Value
        {
            get { return this.value; }
            set
            {
                this.value = value;
                this.RaisePropertyChanged(() => this.Value);
            }
        }

        public ValueHolder(T value)
        {
            this.Value = value;
        }

        public override bool Equals(object obj)
        {
            if (obj is ValueHolder<T>)
            {
                var vh = obj as ValueHolder<T>;
                return this.Value.Equals(vh.Value);
            }
            return base.Equals(obj);
        }

        public override int GetHashCode()
        {
            return (int)(this.Value.GetHashCode() * Math.PI);
        }
    }
}
