﻿namespace DomainWatcher.Tests.Architecture.Components.DataBinding
{
    using DomainWatcher.Architecture.Components;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using System;
    using System.ComponentModel;
    using DomainWatcher.Tests.Mocks;
    using DomainWatcher.Architecture.Components.DataBinding;

    /// <summary>
    ///This is a test class for PropertyBinderTest and is intended
    ///to contain all PropertyBinderTest Unit Tests
    ///</summary>
    [TestClass()]
    public class PropertyBinderTest
    {
        #region Test Generated Code

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion

        #endregion

        /// <summary>
        /// A test for RegisterBinding
        ///</summary>
        [TestMethod()]
        public void RegisterBindingTest()
        {
            ValueHolder<int> leftSideObject = new ValueHolder<int>(0);
            ValueHolder<int> rightSideObject = new ValueHolder<int>(0);
            PropertyBinder target = new PropertyBinder(leftSideObject, rightSideObject, BindingDirection.TwoWay);

            target.RegisterBinding("Value", "Value");

            leftSideObject.Value = 123;

            Assert.AreEqual(leftSideObject.Value, 123);
            Assert.AreEqual(leftSideObject, rightSideObject);

            rightSideObject.Value = 789;

            Assert.AreEqual(rightSideObject.Value, 789);
            Assert.AreEqual(rightSideObject, leftSideObject);
        }

        /// <summary>
        /// A test for UnregisterBinding
        ///</summary>
        [TestMethod()]
        public void UnregisterBindingTest()
        {
            ValueHolder<int> leftSideObject = new ValueHolder<int>(0);
            ValueHolder<int> rightSideObject = new ValueHolder<int>(0);
            PropertyBinder target = new PropertyBinder(leftSideObject, rightSideObject, BindingDirection.TwoWay);

            target.RegisterBinding("Value", "Value");
            target.UnregisterBinding("Value", "Value");

            leftSideObject.Value = 123;

            Assert.AreEqual(leftSideObject.Value, 123);
            Assert.AreNotEqual(leftSideObject, rightSideObject);

            rightSideObject.Value = 789;

            Assert.AreEqual(rightSideObject.Value, 789);
            Assert.AreNotEqual(rightSideObject, leftSideObject);
        }
    }
}
