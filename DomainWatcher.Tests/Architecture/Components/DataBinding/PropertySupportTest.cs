﻿namespace DomainWatcher.Tests.Architecture.Components.DataBinding
{
    using System;
    using System.Collections.Generic;
    using System.Linq.Expressions;
    using DomainWatcher.Architecture.Components;
    using DomainWatcher.Tests.Mocks;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using DomainWatcher.Infrastructure;

    /// <summary>
    ///This is a test class for PropertySupportTest and is intended
    ///to contain all PropertySupportTest Unit Tests
    ///</summary>
    [TestClass()]
    public class PropertySupportTest
    {
        #region Test Generated Code

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion

        #endregion

        #region Helper

        public int MyBigNamedPropertyForTestCase { get; set; }

        #endregion

        /// <summary>
        ///A test for ExchangePropertyData
        ///</summary>
        [TestMethod()]
        public void ExchangePropertyDataTest()
        {
            object donor = new ValueHolder<int>(123);
            string donorPropertyName = "Value";
            object receiver = new ValueHolder<int>(0);
            string receiverPropertyName = "Value";

            PropertySupport.ExchangePropertyData(donor, donorPropertyName, receiver, receiverPropertyName);

            Assert.AreEqual(donor, receiver);
        }

        /// <summary>
        ///A test for ExtractPropertyName
        ///</summary>
        [TestMethod()]
        public void ExtractPropertyNameTestHelper()
        {
            string expected = "MyBigNamedPropertyForTestCase";
            string actual = PropertySupport.ExtractPropertyName(() => this.MyBigNamedPropertyForTestCase);

            Assert.AreEqual(expected, actual);
        }
    }
}
