﻿namespace DomainWatcher.Tests.Infrastructure
{
    using DomainWatcher.Infrastructure;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using System;
    using System.Linq.Expressions;
    using System.Collections.Generic;
    using System.IO;
    using DomainWatcher.Tests.Utilities;

    /// <summary>
    ///This is a test class for ValidationSupportTest and is intended
    ///to contain all ValidationSupportTest Unit Tests
    ///</summary>
    [TestClass()]
    public class ValidationSupportTest
    {
        #region Test Generated Code

        private TestContext testContextInstance;

        /// <summary>
        /// Gets or sets the test context which provides
        /// information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion

        #endregion

        [TestMethod]
        public void ThrowIfArgumentNull_With_A_Valid_Argument_Test()
        {
            string argument = Path.GetRandomFileName();

            AssertException.IsNotThrown(() =>
                ValidationSupport.ThrowIfArgumentNull(() => argument));
        }

        [TestMethod]
        public void ThrowIfArgumentNull_With_An_Invalid_Argument_Test()
        {
            string argument = null;

            AssertException.IsThrown<ArgumentNullException>(() =>
                ValidationSupport.ThrowIfArgumentNull(() => argument));
        }

        [TestMethod]
        public void ThrowIfArgumentNull_With_A_Null_Argument_Test()
        {
            Expression<Func<object>> expression = null;

            AssertException.IsThrown<ArgumentNullException>(() =>
                ValidationSupport.ThrowIfArgumentNull(expression));
        }

        [TestMethod]
        public void ThrowIfArgumentOutOfRange_With_A_Valid_Argument_Test()
        {
            string instance = Path.GetRandomFileName();
            string[] collection = new string[] { instance };

            AssertException.IsNotThrown(() =>
                ValidationSupport.ThrowIfArgumentOutOfRange(() => instance, collection));
        }

        [TestMethod]
        public void ThrowIfArgumentOutOfRange_With_An_Invalid_Argument_Test()
        {
            string instance = Path.GetRandomFileName();
            string[] collection = new string[] { Path.GetRandomFileName() };

            AssertException.IsThrown<ArgumentOutOfRangeException>(() =>
                ValidationSupport.ThrowIfArgumentOutOfRange(() => instance, collection));
        }
    }
}
