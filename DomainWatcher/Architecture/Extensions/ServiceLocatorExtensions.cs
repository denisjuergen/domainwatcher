﻿namespace DomainWatcher.Architecture.Extensions
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using DomainWatcher.Architecture.Contracts;

    public static class ServiceLocatorExtensions
    {
        public static T GetService<T>(this IServiceLocator serviceLocator)
        {
            return (T)(serviceLocator.GetService(typeof(T)));
        }

        public static T GetService<T>(this IServiceLocator serviceLocator, string key)
        {
            return (T)(serviceLocator.GetService(typeof(T), key));
        }

        public static T GetInstance<T>(this IServiceLocator InstanceLocator)
        {
            return (T)(InstanceLocator.GetInstance(typeof(T)));
        }

        public static T GetInstance<T>(this IServiceLocator InstanceLocator, string key)
        {
            return (T)(InstanceLocator.GetInstance(typeof(T), key));
        }
    }
}
