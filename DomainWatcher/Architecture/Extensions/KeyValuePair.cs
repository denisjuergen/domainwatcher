﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DomainWatcher.Architecture.Extensions
{
    public static class KeyValuePair
    {
        public static KeyValuePair<TKey, TValue> Create<TKey, TValue>(TKey key, TValue value)
        {
            return new KeyValuePair<TKey, TValue>(key, value);
        }
    }
}
