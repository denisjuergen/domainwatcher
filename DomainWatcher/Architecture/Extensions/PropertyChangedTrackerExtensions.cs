﻿namespace DomainWatcher.Architecture.Extensions
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using DomainWatcher.Architecture.Components.DataBinding;
    using System.Linq.Expressions;
    using System.ComponentModel;
    using DomainWatcher.Infrastructure;
    using System.Reflection;

    public static class PropertyChangedTrackerExtensions
    {
        public static void TrackPropertyChange<TFrom, TProperty>(this PropertyChangedTracker propertyChangedTracker, TFrom from, Expression<Func<TFrom, TProperty>> expression, Action<TProperty> valueDelegate)
            where TFrom : INotifyPropertyChanged
        {
            propertyChangedTracker.TrackPropertyChange(from, PropertyChangedTrackerExtensions.ExtractFieldName(expression), o => valueDelegate((TProperty)(o)), false);
        }

        public static void TrackPropertyChange<TFrom, TProperty>(this PropertyChangedTracker propertyChangedTracker, TFrom from, Expression<Func<TFrom, TProperty>> expression, Action<TProperty> valueDelegate, bool enableRecursion)
            where TFrom : INotifyPropertyChanged
        {
            propertyChangedTracker.TrackPropertyChange(from, PropertyChangedTrackerExtensions.ExtractFieldName(expression), o => valueDelegate((TProperty)(o)), enableRecursion);
        }

        public static void UnTrackPropertyChanged<TFrom, TProperty>(this PropertyChangedTracker propertyChangedTracker, TFrom from, Expression<Func<TFrom, TProperty>> expression)
            where TFrom : INotifyPropertyChanged
        {
            propertyChangedTracker.UnTrackPropertyChange(from, PropertyChangedTrackerExtensions.ExtractFieldName(expression));
        }

        public static void UnTrackPropertyChanged<TFrom, TProperty>(this PropertyChangedTracker propertyChangedTracker, TFrom from, Expression<Func<TFrom, TProperty>> expression, Action<TProperty> valueDelegate)
            where TFrom : INotifyPropertyChanged
        {
            propertyChangedTracker.UnTrackPropertyChange(from, PropertyChangedTrackerExtensions.ExtractFieldName(expression), o => valueDelegate((TProperty)(o)));
        }

        private static string ExtractFieldName<TFrom, TProperty>(Expression<Func<TFrom, TProperty>> expression)
        {
            ValidationSupport.ThrowIfArgumentNull(() => expression);

            MemberExpression memberExpression = expression.Body as MemberExpression;
            if (memberExpression == null)
            {
                throw new ArgumentException(string.Format(
                    "Expression '{0}' refers to a method, not a property.",
                    expression.ToString()));
            }
            if (memberExpression.Member.ReflectedType != typeof(TFrom))
            {
                throw new ArgumentException(string.Format(
                    "Expression '{0}' does not refers to a member of the type '{1}'.",
                    expression.ToString(), typeof(TFrom)));
            }

            PropertyInfo propertyInfo = memberExpression.Member as PropertyInfo;
            if (propertyInfo == null)
            {
                throw new ArgumentException(string.Format(
                    "Expression '{0}' refers to a field, not a property.",
                    expression.ToString()));
            }
            if (propertyInfo.GetGetMethod(true).IsStatic)
            {
                throw new ArgumentException(string.Format(
                    "Expression '{0}' refers to a static property, not a class member.",
                    expression.ToString()));
            }
            if (memberExpression.Member.ReflectedType != typeof(TFrom))
            {
                throw new ArgumentException(string.Format(
                    "Expression '{0}' does not refers to a member of the class '{1}'.",
                    expression.ToString(), typeof(TFrom)));
            }

            return memberExpression.Member.Name;
        }
    }
}
