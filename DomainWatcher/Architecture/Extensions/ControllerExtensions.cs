﻿namespace DomainWatcher.Architecture.Extensions
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using DomainWatcher.Architecture.Contracts;

    public static class ControllerExtensions
    {
        public static TView GetFirstOrCreateView<TView, TPresenter>(this IController<TView, TPresenter> controller)
            where TView : class, IView<TPresenter, TView>
            where TPresenter : class, IPresenter<TView, TPresenter>
        {
            return controller.Views.FirstOrDefault() ?? controller.CreateView();
        }
    }
}
