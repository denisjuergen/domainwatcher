﻿namespace DomainWatcher.Architecture.Extensions
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using DomainWatcher.Architecture.Components.Modules;
    using DomainWatcher.Architecture.Components;

    public static class ModuleManagerExtensions
    {
        public static void Bootstrap(this IModuleManager moduleManager)
        {
            var moduleCatalog = ServiceLocator.Current.GetInstance<IModuleCatalog>();
            IEnumerable<string> moduleNames = moduleCatalog.Modules.Select(m => m.ModuleName);

            moduleManager.LoadModules(moduleNames);
        }

        public static void LoadModules(this IModuleManager moduleManager, IEnumerable<string> moduleNames)
        {
            foreach (var moduleName in moduleNames)
            {
                moduleManager.LoadModule(moduleName);
            }
        }

        public static IModuleCatalog RegisterModule<TModule>(this IModuleCatalog moduleCatalog) where TModule : IModule
        {
            return moduleCatalog.RegisterModule(typeof(TModule));
        }

        public static IModuleCatalog RegisterModule<TModule>(this IModuleCatalog moduleCatalog, string moduleName) where TModule : IModule
        {
            return moduleCatalog.RegisterModule(typeof(TModule), moduleName);
        }

        public static IModuleCatalog RegisterModule(this IModuleCatalog moduleCatalog, Type moduleType)
        {
            return moduleCatalog.RegisterModule(moduleType, moduleType.Name);
        }

        public static IModuleCatalog RegisterModule(this IModuleCatalog moduleCatalog, Type moduleType, string moduleName)
        {
            moduleCatalog.AddModule(new ModuleData(moduleType, moduleName));
            return moduleCatalog;
        }
    }
}
