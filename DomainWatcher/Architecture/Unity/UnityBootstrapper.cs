﻿namespace DomainWatcher.Architecture.Unity
{
    using DomainWatcher.Architecture.Components;
    using DomainWatcher.Architecture.Contracts;
    using Microsoft.Practices.Unity;
    using DomainWatcher.Architecture.Components.Regions;
    using DomainWatcher.Architecture.Components.Modules;
    using DomainWatcher.Architecture.Components.Logging;
    using DomainWatcher.Architecture.Components.Events;

    public abstract class UnityBootstrapper : Boostrapper
    {
        #region Properties

        public IUnityContainer Container { get { return this.container; } }

        #endregion

        #region Public Methods

        public override void Run()
        {
            // Unity Container Configuration
            var container = this.CreateContainer();
            this.ConfigureContainer(container);
            this.container = container; // After setup, make it available for the bootstrapper

            base.Run();
        }

        #endregion

        #region Protected Methods

        protected virtual IUnityContainer CreateContainer()
        {
            return new UnityContainer();
        }

        protected override IServiceLocator CreateServiceLocator()
        {
            return container.Resolve<IServiceLocator>();
        }

        protected virtual void ConfigureContainer(IUnityContainer container)
        {
            // Service Locator Registration
            container.RegisterType<IServiceLocator, UnityServiceLocator>(new ContainerControlledLifetimeManager());

            // Logger Registration
            container.RegisterType<ILogger, DebugWindowLogger>(new ContainerControlledLifetimeManager());

            // Region Related Registration
            container.RegisterType<IRegion, Region>()
                     .RegisterType<IRegionManager, RegionManager>(new ContainerControlledLifetimeManager())
                     .RegisterType<IRegionViewRegistry, RegionViewRegistry>(new ContainerControlledLifetimeManager())
                     .RegisterType<IRegionAdapterLocator, RegionAdapterLocator>(new ContainerControlledLifetimeManager());

            // Modules Related Registration
            container.RegisterType<IModuleCatalog, ModuleCatalog>(new ContainerControlledLifetimeManager())
                     .RegisterType<IModuleManager, ModuleManager>(new ContainerControlledLifetimeManager());

            // Eventing Interface
            container.RegisterType<IEventAggregator, EventAggregator>(new ContainerControlledLifetimeManager());
        }

        #endregion

        #region Private Fields

        private IUnityContainer container;

        #endregion
    }
}
