﻿namespace DomainWatcher.Architecture.Unity
{
    using System;
    using DomainWatcher.Architecture.Components;
    using Microsoft.Practices.Unity;

    public sealed class UnityServiceLocator : ServiceLocator
    {
        #region Constructors

        public UnityServiceLocator(IUnityContainer container)
        {
            this.container = container;
        }

        #endregion

        #region ServiceLocator Members

        public override object GetService(Type serviceType, string name)
        {
            return this.container.Resolve(serviceType, name);
        }

        #endregion

        #region Private Fields

        private readonly IUnityContainer container;

        #endregion
    }
}
