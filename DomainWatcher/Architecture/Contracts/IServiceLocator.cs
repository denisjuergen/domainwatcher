﻿namespace DomainWatcher.Architecture.Contracts
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    public interface IServiceLocator : IServiceProvider
    {
        /// <summary>
        /// Gets the service object of the specified type registered with the specified name.
        /// </summary>
        /// <param name="serviceType">An object that specifies the type of service object to get.</param>
        /// <param name="name">A naming reference to the instance of the service object to get.</param>
        /// <returns>
        /// A service object of type <paramref name="serviceType"/>.-or- null if there is no service object of type <paramref name="serviceType"/>.
        /// </returns>
        object GetService(Type serviceType, string name);

        object GetInstance(Type serviceType);
        object GetInstance(Type serviceType, string name);
    }
}
