﻿namespace DomainWatcher.Architecture.Contracts
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Linq;
    using System.Text;

    public interface IPresenter<TView, TPresenter>
        where TView : IView<TPresenter, TView>
        where TPresenter : IPresenter<TView, TPresenter>
    {
        IEnumerable<TView> Views { get; }

        void AttachView(TView view);
        void DetachView(TView view);
    }
}
