﻿namespace DomainWatcher.Architecture.Contracts
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Linq;
    using System.Text;

    public interface IModel : IDataErrorInfo, INotifyPropertyChanged
    {
        bool IsValid { get; }
        void Validate();
    }
}
