﻿namespace DomainWatcher.Architecture.Contracts
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Linq.Expressions;
    using System.ComponentModel;

    public interface IValidator<TModel> : IDataErrorInfo
    {
        int ErrorCount { get; }
        void ResetErrors();
        void Validate<TProperty>(Expression<Func<TModel, TProperty>> expression, Predicate<TProperty> predicate, string invalidMessage);
    }
}
