﻿namespace DomainWatcher.Architecture.Contracts
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    public interface IController<TView, TPresenter>
        where TView : IView<TPresenter, TView>
        where TPresenter : IPresenter<TView, TPresenter>
    {
        IEnumerable<TView> Views { get; }
        TPresenter Presenter { get; }

        TView CreateView();
        bool DestroyView(TView view);
    }
}
