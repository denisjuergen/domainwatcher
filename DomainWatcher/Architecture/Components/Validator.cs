﻿namespace DomainWatcher.Architecture.Components
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using DomainWatcher.Architecture.Contracts;
    using System.Linq.Expressions;
    using DomainWatcher.Infrastructure;

    public class Validator<TModel> : IValidator<TModel>
    {
        #region Constructors

        public Validator(TModel model)
        {
            this.model = model;
            this.errors = new Dictionary<string, string>();
        }

        #endregion

        #region Properties

        public int ErrorCount { get { return this.errors.Count; } }

        #endregion

        #region Methods

        public void ResetErrors()
        {
            this.errors.Clear();
        }

        #endregion

        #region IValidator<TModel> Members

        public void Validate<TProperty>(Expression<Func<TModel, TProperty>> expression, Predicate<TProperty> predicate, string invalidMessage)
        {
            var propertyName = PropertySupport.ExtractPropertyName(expression);
            var propertyGetMethod = expression.Compile();
            if (!predicate(propertyGetMethod(this.model)))
            {
                this.errors.Add(propertyName, invalidMessage);
            }
        }

        #endregion

        #region IDataErrorInfo Members

        public string Error
        {
            get { return this.ToString(); }
        }

        public string this[string columnName]
        {
            get
            {
                if (this.errors.ContainsKey(columnName))
                {
                    return this.errors[columnName];
                }
                return null;
            }
        }

        #endregion

        #region Overrides

        public override string ToString()
        {
            StringBuilder error = new StringBuilder();
            foreach (var message in this.errors.Values)
            {
                error.AppendLine(message);
            }
            return error.ToString();
        }

        #endregion

        #region Fields

        private readonly TModel model;
        private readonly Dictionary<string, string> errors;

        #endregion
    }
}
