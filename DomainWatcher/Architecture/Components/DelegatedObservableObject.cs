﻿namespace DomainWatcher.Architecture.Components
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Linq;
    using System.Text;
    using System.Linq.Expressions;

    public sealed class DelegatedObservableObject : ObservableObject
    {
        #region ObservableObject Overrides

        new public void RaisePropertyChanged<T>(Expression<Func<T>> expression)
        {
            base.RaisePropertyChanged<T>(expression);
        }

        new public void RaisePropertyChanged(string propertyName)
        {
            base.RaisePropertyChanged(propertyName);
        }

        #endregion
    }
}
