﻿namespace DomainWatcher.Architecture.Components.Modules
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using DomainWatcher.Properties;
    using System.Globalization;

    public sealed class ModuleData
    {
        public Type ModuleType { get; set; }
        public string ModuleName { get; set; }

        public ModuleData(Type type, string name)
        {
            if (!typeof(IModule).IsAssignableFrom(type))
            {
                throw new ArgumentException(string.Format(CultureInfo.CurrentCulture, Resources.ModuleDoNotInheritsFromIModuleException, type));
            }

            this.ModuleType = type;
            this.ModuleName = name;
        }
    }
}
