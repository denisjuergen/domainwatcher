﻿namespace DomainWatcher.Architecture.Components.Modules
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    public sealed class ModuleLoadedEventArgs : EventArgs
    {
        public ModuleData ModuleData { get; private set; }
        public Exception Error { get; private set; }
        public bool HasLoadedSuccessfully { get { return this.Error == null; } }
        public bool IsErrorHandled { get; set; }

        public ModuleLoadedEventArgs(ModuleData moduleData, Exception error)
        {
            this.ModuleData = moduleData;
            this.Error = error;
        }
    }
}
