﻿namespace DomainWatcher.Architecture.Components.Modules
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    public interface IModuleManager
    {
        event EventHandler<ModuleLoadedEventArgs> ModuleLoaded;

        void LoadModule(string moduleName);
    }
}
