﻿namespace DomainWatcher.Architecture.Components.Modules
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using DomainWatcher.Infrastructure;

    public class ModuleCatalog : IModuleCatalog
    {
        #region Constructors

        public ModuleCatalog()
        {
            this.modulesData = new HashSet<ModuleData>();
        }

        #endregion

        #region IModuleCatalog Members

        public virtual IEnumerable<ModuleData> Modules
        {
            get { return this.modulesData; }
        }

        public void AddModule(ModuleData moduleData)
        {
            ValidationSupport.ThrowIfArgumentNull(() => moduleData);

            this.modulesData.Add(moduleData);
        }

        #endregion

        #region Private Fields

        private HashSet<ModuleData> modulesData;

        #endregion
    }
}
