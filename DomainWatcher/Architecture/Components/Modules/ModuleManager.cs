﻿namespace DomainWatcher.Architecture.Components.Modules
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using DomainWatcher.Infrastructure;
    using System.Globalization;
    using DomainWatcher.Properties;

    public sealed class ModuleManager : IModuleManager
    {
        #region Constructors

        public ModuleManager(IModuleCatalog moduleCatalog)
        {
            this.moduleCatalog = moduleCatalog;
        }

        #endregion

        #region IModuleManager Members

        public event EventHandler<ModuleLoadedEventArgs> ModuleLoaded;

        public void LoadModule(string moduleName)
        {
            ValidationSupport.ThrowIfArgumentNull(() => moduleName);

            ModuleData moduleData = this.moduleCatalog.Modules.FirstOrDefault(m => m.ModuleName.Equals(moduleName));
            if (moduleData == null)
            {
                throw new KeyNotFoundException(string.Format(CultureInfo.CurrentCulture, Resources.ModuleNameNotFoundException, moduleName));
            }

            Exception moduleLoadError = null;
            try
            {
                IModule module = (IModule)ServiceLocator.Current.GetInstance(moduleData.ModuleType);
                module.Initialize();
            }
            catch (Exception exception)
            {
                moduleLoadError = exception;
            }
            this.RaiseModuleLoaded(moduleData, moduleLoadError);
        }

        private void RaiseModuleLoaded(ModuleData moduleData, Exception error)
        {
            ModuleLoadedEventArgs e = new ModuleLoadedEventArgs(moduleData, error);
            if (this.ModuleLoaded != null)
            {
                this.ModuleLoaded(this, e);
            }
            if (!e.HasLoadedSuccessfully && !e.IsErrorHandled)
            {
                throw e.Error;
            }
        }

        #endregion

        private readonly IModuleCatalog moduleCatalog;
    }
}
