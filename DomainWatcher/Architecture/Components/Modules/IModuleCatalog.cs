﻿namespace DomainWatcher.Architecture.Components.Modules
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    public interface IModuleCatalog
    {
        IEnumerable<ModuleData> Modules { get; }

        void AddModule(ModuleData moduleData);
    }
}
