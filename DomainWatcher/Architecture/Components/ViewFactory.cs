﻿namespace DomainWatcher.Architecture.Components
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using DomainWatcher.Architecture.Contracts;
    using DomainWatcher.Architecture.Extensions;
    using Microsoft.Practices.Unity;

    public abstract class ViewFactory<TView, TPresenter> : IViewFactory<TView, TPresenter>
        where TView : IView<TPresenter, TView>
        where TPresenter : IPresenter<TView, TPresenter>
    {
        #region IViewFactory<TView,TPresenter> Members

        public TView CreateView()
        {
            return ServiceLocator.Current.GetInstance<TView>();
        }

        #endregion
    }
}
