﻿namespace DomainWatcher.Architecture.Components
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using DomainWatcher.Architecture.Contracts;

    public abstract class ServiceLocator : IServiceLocator
    {
        #region Public Properties

        public static IServiceLocator Current
        {
            get { return m_currentServiceLocator; }
        }

        #endregion

        #region Public Methods

        public static void SetServiceLocator(IServiceLocator serviceLocator)
        {
            m_currentServiceLocator = serviceLocator;
        }

        #endregion

        #region IServiceLocator Members

        public abstract object GetService(Type serviceType, string name);

        #endregion

        #region IServiceProvider Members

        public object GetService(Type serviceType)
        {
            return this.GetService(serviceType, null);
        }

        public object GetInstance(Type serviceType)
        {
            return this.GetService(serviceType, null);
        }

        public object GetInstance(Type serviceType, string name)
        {
            return this.GetService(serviceType, name);
        }

        #endregion

        #region Private Fields

        private static IServiceLocator m_currentServiceLocator;

        #endregion
    }
}
