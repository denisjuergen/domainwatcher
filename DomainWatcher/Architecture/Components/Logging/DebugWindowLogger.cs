﻿namespace DomainWatcher.Architecture.Components.Logging
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using DomainWatcher.Architecture.Contracts;
    using System.Diagnostics;
    using System.Globalization;
    using DomainWatcher.Properties;

    public class DebugWindowLogger : ILogger
    {
        #region Constructors

        public DebugWindowLogger()
        {
            this.Listeners = new List<ILogListener>();
        }

        #endregion

        #region ILogger Members

        public void Log(string message, LogLevel logLevel)
        {
            Debug.WriteLine(string.Format(CultureInfo.CurrentCulture, Resources.DebugWindowLoggerMessagePattern, DateTime.Now, logLevel, message));

            foreach (var listener in this.Listeners)
            {
                listener.Log(message, logLevel);
            }
        }

        #endregion

        #region ILogger Members

        public IList<ILogListener> Listeners { get; private set; }

        #endregion
    }
}
