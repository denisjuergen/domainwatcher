﻿namespace DomainWatcher.Architecture.Components.Logging
{
    using DomainWatcher.Architecture.Components.Logging;
    using System.Collections.Generic;

    public interface ILogger : ILogListener
    {
        IList<ILogListener> Listeners { get; }
    }
}
