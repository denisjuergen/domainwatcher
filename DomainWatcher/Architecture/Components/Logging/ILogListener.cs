﻿namespace DomainWatcher.Architecture.Components.Logging
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    public interface ILogListener
    {
        void Log(string message, LogLevel logLevel);
    }
}
