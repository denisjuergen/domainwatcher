﻿namespace DomainWatcher.Architecture.Components.DataBinding
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    internal sealed class BindingLatch
    {
        #region Public Properties

        public bool IsLatched { get { return this.isLatched; } }
        public object SyncRoot { get { return this.syncRoot; } }

        #endregion

        #region Public Methods

        public void Execute(Action action)
        {
            lock (this.syncRoot)
            {
                if (!this.isLatched)
                {
                    this.isLatched = true;

                    action();

                    this.isLatched = false;
                }
            }
        }

        #endregion

        #region Private Fields

        private bool isLatched = false;
        private object syncRoot = new object();

        #endregion
    }
}
