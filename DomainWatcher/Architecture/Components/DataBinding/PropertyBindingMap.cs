﻿namespace DomainWatcher.Architecture.Components.DataBinding
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    internal sealed class PropertyBindingMap
    {
        #region Constructors

        private PropertyBindingMap(string left, string right)
        {
            this.Left = left;
            this.Right = right;
        }

        #endregion

        #region Public Methods

        public static PropertyBindingMap Map(string left, string right)
        {
            return new PropertyBindingMap(left, right);
        }

        #endregion

        #region Public Properties

        public string Left { get; private set; }
        public string Right { get; private set; }

        #endregion
    }
}
