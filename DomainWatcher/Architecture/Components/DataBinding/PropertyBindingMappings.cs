﻿namespace DomainWatcher.Architecture.Components.DataBinding
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    internal sealed class PropertyBindingMappings
    {
        #region Constructors

        internal PropertyBindingMappings()
        {
            this.maps = new HashSet<PropertyBindingMap>();
        }

        #endregion

        #region Public Methods

        public void RegisterMap(string left, string right)
        {
            this.maps.Add(PropertyBindingMap.Map(left, right));
        }

        public void UnregisterMap(string left, string right)
        {
            this.maps.RemoveWhere(b => b.Left == left && b.Right == right);
        }

        public bool HasBindingMap(string property, PropertyBindingSide propertySide)
        {
            return this.GetBindingMap(property, propertySide) != null;
        }

        public string GetBindingPropertyName(string property, PropertyBindingSide propertySide)
        {
            PropertyBindingMap binding = this.GetBindingMap(property, propertySide);

            if (binding == null)
            {
                throw new ArgumentException(string.Format(
                    "No property binding map registered for the '{0}' side property name '{1}'",
                    propertySide, property
                    ));
            }

            return propertySide == PropertyBindingSide.Left ? binding.Left : binding.Right;
        }

        #endregion

        #region Private Methods

        private PropertyBindingMap GetBindingMap(string property, PropertyBindingSide propertySide)
        {
            return this.maps.SingleOrDefault(b => (propertySide == PropertyBindingSide.Right ? b.Left == property : b.Right == property));
        }

        #endregion

        #region Private Fields

        private readonly HashSet<PropertyBindingMap> maps;

        #endregion
    }
}
