﻿namespace DomainWatcher.Architecture.Components.DataBinding
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Linq.Expressions;
    using System.ComponentModel;
    using DomainWatcher.Architecture.Components.Collections;
    using DomainWatcher.Properties;
    using DomainWatcher.Infrastructure;

    public sealed class PropertyChangedTracker
    {
        #region Constructors

        public PropertyChangedTracker()
        {
            this.latch = new BindingLatch();
            this.valueDelegates = new ListDictionary<object, TrackingMetaData>();
        }

        #endregion

        #region Public Methods

        public void TrackPropertyChange(object from, string propertyName, Action<object> valueDelegate, bool enableRecursion)
        {
            var observable = from as INotifyPropertyChanged;
            if (observable == null)
            {
                ValidationSupport.ThrowArgumentException(() => from, Resources.ObjectIsNotNotificationObject);
            }
            if (!this.valueDelegates.ContainsKey(from))
            {
                observable.PropertyChanged += this.ObservePropertyChange;
            }
            if (!this.valueDelegates[from].Any(x => x.PropertyName.Equals(propertyName) && x.ValueDelegate.Equals(valueDelegate)))
            {
                this.valueDelegates.Add(from, TrackingMetaData.Create(propertyName, valueDelegate, enableRecursion));
            }
        }

        public void UnTrackPropertyChange(object from)
        {
            var observable = from as INotifyPropertyChanged;
            if (observable != null)
            {
                observable.PropertyChanged -= this.ObservePropertyChange;
            }
            this.valueDelegates.Remove(from);
        }

        public void UnTrackPropertyChange(object from, string propertyName)
        {
            var metaDatas = this.valueDelegates[from].Where(x => x.PropertyName.Equals(propertyName));
            if (metaDatas.Any())
            {
                foreach (var metaData in metaDatas)
                {
                    this.valueDelegates[from].Remove(metaData);
                }
            }
            if (!this.valueDelegates[from].Any())
            {
                this.UnTrackPropertyChange(from);
            }
        }

        public void UnTrackPropertyChange(object from, string propertyName, Action<object> valueDelegate)
        {
            var metaData = this.valueDelegates[from].FirstOrDefault(x => x.PropertyName.Equals(propertyName) && x.ValueDelegate.Equals(valueDelegate));
            if (metaData != null)
            {
                this.valueDelegates.Remove(from, metaData);
            }
            if (!this.valueDelegates[from].Any())
            {
                this.UnTrackPropertyChange(from);
            }
        }

        #endregion

        #region Private Methods

        private void ObservePropertyChange(object sender, PropertyChangedEventArgs e)
        {
            var metaDatas = this.valueDelegates[sender].Where(x => x.PropertyName.Equals(e.PropertyName));
            if (metaDatas.Any())
            {
                var type = sender.GetType();
                var property = type.GetProperty(e.PropertyName);
                var propertyValue = property.GetValue(sender, null);
                foreach (var metaData in metaDatas)
                {
                    if (metaData.Recursive)
                    {
                        metaData.ValueDelegate(propertyValue);
                    }
                    else
                    {
                        this.latch.Execute(() => metaData.ValueDelegate(propertyValue));
                    }
                }
            }
        }

        #endregion

        #region Private Fields

        [ThreadStatic]
        private readonly BindingLatch latch;
        private readonly ListDictionary<object, TrackingMetaData> valueDelegates;

        #endregion

        private class TrackingMetaData
        {
            public bool Recursive { get; private set; }
            public string PropertyName { get; private set; }
            public Action<object> ValueDelegate { get; private set; }

            private TrackingMetaData()
            {

            }

            public static TrackingMetaData Create(string propertyName, Action<object> valueDelegate, bool recursive)
            {
                return new TrackingMetaData { PropertyName = propertyName, ValueDelegate = valueDelegate, Recursive = recursive };
            }
        }
    }
}