﻿namespace DomainWatcher.Architecture.Components.DataBinding
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Linq;
    using System.Reflection;
    using System.Text;
    using DomainWatcher.Infrastructure;

    public sealed class PropertyBinder : IDisposable
    {
        #region Constructors

        public PropertyBinder(INotifyPropertyChanged leftSideObject, object rightSideObject, BindingDirection bindingDirection)
        {
            this.leftSideObject = leftSideObject;
            this.rightSideObject = rightSideObject;
            this.bindingDirection = bindingDirection;

            this.bindingMaps = new PropertyBindingMappings();
            this.latch = new BindingLatch();

            this.leftSideObject.PropertyChanged += this.LeftSideObservePropertyChange;

            if (this.rightSideObject is INotifyPropertyChanged)
            {
                (this.rightSideObject as INotifyPropertyChanged).PropertyChanged += this.RightSideObservePropertyChange;
            }
        }

        #endregion

        #region Public Methods

        public void RegisterBinding(string leftSideProperty, string rightSideProperty)
        {
            if (this.leftSideObject.GetType().GetProperty(leftSideProperty) == null)
            {
                throw new ArgumentException(string.Format(
                    "'{0}' is not a valid property name for object of type '{1}'",
                    leftSideProperty, this.leftSideObject.GetType().Name
                    ));
            }

            if (this.rightSideObject.GetType().GetProperty(rightSideProperty) == null)
            {
                throw new ArgumentException(string.Format(
                    "'{0}' is not a valid property name for object of type '{1}'",
                    rightSideProperty, this.rightSideObject.GetType().Name
                    ));
            }

            this.bindingMaps.RegisterMap(leftSideProperty, rightSideProperty);
        }

        public void UnregisterBinding(string leftSideProperty, string rightSideProperty)
        {
            this.bindingMaps.UnregisterMap(leftSideProperty, rightSideProperty);
        }

        #endregion

        #region IDisposable Members

        public void Dispose()
        {
            if (!this.disposed)
            {
                this.leftSideObject.PropertyChanged -= this.LeftSideObservePropertyChange;

                if (this.rightSideObject is INotifyPropertyChanged)
                {
                    (this.rightSideObject as INotifyPropertyChanged).PropertyChanged -= this.RightSideObservePropertyChange;
                }

                this.leftSideObject = null;
                this.rightSideObject = null;

                this.disposed = true;
            }
            GC.SuppressFinalize(this);
        }

        #endregion

        #region Private Methods

        private void RightSideObservePropertyChange(object sender, PropertyChangedEventArgs e)
        {
            if (this.bindingDirection == BindingDirection.TwoWay &&
                this.bindingMaps.HasBindingMap(e.PropertyName, PropertyBindingSide.Left))
            {
                this.latch.Execute(() =>
                {
                    var donor = this.rightSideObject;
                    var donorProperty = this.bindingMaps.GetBindingPropertyName(e.PropertyName, PropertyBindingSide.Left);
                    var receiver = this.leftSideObject;
                    var receiverProperty = e.PropertyName;

                    PropertySupport.ExchangePropertyData(donor, donorProperty, receiver, receiverProperty);
                });
            }
        }

        private void LeftSideObservePropertyChange(object sender, PropertyChangedEventArgs e)
        {
            if (this.bindingMaps.HasBindingMap(e.PropertyName, PropertyBindingSide.Right))
            {
                this.latch.Execute(() =>
                {
                    var donor = this.leftSideObject;
                    var donorProperty = e.PropertyName;
                    var receiver = this.rightSideObject;
                    var receiverProperty = this.bindingMaps.GetBindingPropertyName(e.PropertyName, PropertyBindingSide.Right);

                    PropertySupport.ExchangePropertyData(donor, donorProperty, receiver, receiverProperty);
                });
            }
        }

        #endregion

        #region Private Fields

        private bool disposed = false;
        private INotifyPropertyChanged leftSideObject;
        private object rightSideObject;
        private BindingDirection bindingDirection;
        private PropertyBindingMappings bindingMaps;
        private BindingLatch latch;

        #endregion
    }
}
