﻿namespace DomainWatcher.Architecture.Components.DataBinding
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    internal enum PropertyBindingSide
    {
        Left,
        Right
    }
}
