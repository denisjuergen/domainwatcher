﻿namespace DomainWatcher.Architecture.Components
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Text;
    using System.Windows.Forms;
    using DomainWatcher.Architecture.Contracts;

    public abstract class View<TPresenter, TView> : Form, IView<TPresenter, TView>
        where TPresenter : Presenter<TView, TPresenter>
        where TView : View<TPresenter, TView>
    {
        #region Protected Members

        protected virtual void RaisePropertyChanged<T>(Expression<Func<T>> expression)
        {
            this.delegatedObject.RaisePropertyChanged(expression);
        }

        protected virtual void RaisePropertyChanged(string propertyName)
        {
            this.delegatedObject.RaisePropertyChanged(propertyName);
        }

        #endregion

        #region IView<TPresenter,TView> Members

        public abstract void Initialize();

        #endregion

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged
        {
            add { this.delegatedObject.PropertyChanged += value; }
            remove { this.delegatedObject.PropertyChanged -= value; }
        }

        #endregion

        #region Private Fields

        private readonly DelegatedObservableObject delegatedObject = new DelegatedObservableObject();

        #endregion
    }
}
