﻿namespace DomainWatcher.Architecture.Components.Collections
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Collections;
    using DomainWatcher.Architecture.Extensions;

    public sealed class ListDictionary<TKey, TValue> : IDictionary<TKey, IList<TValue>>
    {
        #region Constructors

        public ListDictionary()
        {
            this.dictionary = new Dictionary<TKey, IList<TValue>>();
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Initializes a list in the collection with the specified key.
        /// </summary>
        /// <param name="key">The list key.</param>
        public void Add(TKey key)
        {
            this.GetListForKey(key);
        }

        /// <summary>
        /// Adds a value to the list specified by the key. The list will be initialized if there's none.
        /// </summary>
        /// <param name="key">The list key.</param>
        /// <param name="value">The value to add to the list.</param>
        public void Add(TKey key, TValue value)
        {
            this.GetListForKey(key)
                .Add(value);
        }

        /// <summary>
        /// Determines whether the specified value exists within a list.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns>
        ///   <c>true</c> if the specified value exists inside any list; otherwise, <c>false</c>.
        /// </returns>
        public bool ContainsValue(TValue value)
        {
            return this.GetFlattenEnumerable()
                       .Any(p => p.Value.Equals(value));
        }

        /// <summary>
        /// Finds all values that meets the value predicate criteria.
        /// </summary>
        /// <param name="valueFilter">The predicate function to filter the values.</param>
        /// <returns>A collection of values after applying the predicate filter.</returns>
        public IEnumerable<TValue> FindAllValues(Predicate<TValue> valueFilter)
        {
            return this.GetFlattenEnumerable()
                       .Where(p => valueFilter(p.Value))
                       .Select(p => p.Value);
        }

        /// <summary>
        /// Finds all values that meets the key predicate criteria.
        /// </summary>
        /// <param name="keyFilter">The predicate function to filter the values by the list key.</param>
        /// <returns>A collection of values after applying the predicate filter.</returns>
        public IEnumerable<TValue> FindAllValuesByKey(Predicate<TKey> keyFilter)
        {
            return this.GetFlattenEnumerable()
                       .Where(p => keyFilter(p.Key))
                       .Select(p => p.Value);
        }

        /// <summary>
        /// Removes the specified value from all the lists.
        /// </summary>
        /// <param name="value">The value to be removed.</param>
        /// <returns>
        ///   <c>true</c> if the value was removed from any list; otherwise, <c>false</c>.
        /// </returns>
        public bool Remove(TValue value)
        {
            bool removed = false;
            foreach (var list in this.dictionary.Values)
            {
                if (list.Remove(value))
                    removed = true;
            }
            return removed;
        }

        /// <summary>
        /// Removes the specified value from the list with the specified key.
        /// </summary>
        /// <param name="key">The list key.</param>
        /// <param name="value">The value to be removed.</param>
        /// <returns>
        ///   <c>true</c> if the value was removed from the list; otherwise, <c>false</c>.
        /// </returns>
        public bool Remove(TKey key, TValue value)
        {
            return this.GetListForKey(key)
                       .Remove(value);
        }

        #endregion

        #region Private Methods

        private IList<TValue> GetListForKey(TKey key)
        {
            if (!this.dictionary.ContainsKey(key))
            {
                this.dictionary.Add(key, new List<TValue>());
            }
            return this.dictionary[key];
        }

        private IEnumerable<KeyValuePair<TKey, TValue>> GetFlattenEnumerable()
        {
            return this.dictionary.Keys.AsQueryable()
                                       .SelectMany(k => this.dictionary[k].Select(v => KeyValuePair.Create(k, v)));
        }

        #endregion

        #region IDictionary<TKey,IList<TValue>> Members

        public ICollection<TKey> Keys
        {
            get { return this.dictionary.Keys; }
        }

        ICollection<IList<TValue>> IDictionary<TKey, IList<TValue>>.Values
        {
            get { return this.dictionary.Values; }
        }

        public IList<TValue> this[TKey key]
        {
            get
            {
                if (!this.dictionary.ContainsKey(key))
                {
                    this.dictionary[key] = new List<TValue>();
                }
                return this.dictionary[key];
            }
            set { this.dictionary[key] = value; }
        }

        /// <summary>
        /// Determines whether the collection contains a list with the specified key.
        /// </summary>
        /// <param name="key">The list key.</param>
        /// <returns>
        ///   <c>true</c> if the collection holds a list with the specified key; otherwise, <c>false</c>.
        /// </returns>
        public bool ContainsKey(TKey key)
        {
            return this.dictionary.ContainsKey(key);
        }

        /// <summary>
        /// Removes the list with specified key from the collection.
        /// </summary>
        /// <param name="key">The list key.</param>
        /// <returns><c>tru</c> if the list was removed; otherwise, <c>false</c></returns>.
        public bool Remove(TKey key)
        {
            return this.dictionary.Remove(key);
        }

        void IDictionary<TKey, IList<TValue>>.Add(TKey key, IList<TValue> value)
        {
            this.dictionary.Add(key, value);
        }

        bool IDictionary<TKey, IList<TValue>>.TryGetValue(TKey key, out IList<TValue> value)
        {
            return this.dictionary.TryGetValue(key, out value);
        }

        #endregion

        #region ICollection<KeyValuePair<TKey,IList<TValue>>> Members

        public int Count
        {
            get { return ((ICollection<KeyValuePair<TKey, IList<TValue>>>)this.dictionary).Count; }
        }

        /// <summary>
        /// Removes all keys and their respective lists from this collection.
        /// </summary>
        public void Clear()
        {
            this.dictionary.Clear();
        }

        void ICollection<KeyValuePair<TKey, IList<TValue>>>.Add(KeyValuePair<TKey, IList<TValue>> item)
        {
            ((ICollection<KeyValuePair<TKey, IList<TValue>>>)this.dictionary).Add(item);
        }

        bool ICollection<KeyValuePair<TKey, IList<TValue>>>.Contains(KeyValuePair<TKey, IList<TValue>> item)
        {
            return ((ICollection<KeyValuePair<TKey, IList<TValue>>>)this.dictionary).Contains(item);
        }

        void ICollection<KeyValuePair<TKey, IList<TValue>>>.CopyTo(KeyValuePair<TKey, IList<TValue>>[] array, int arrayIndex)
        {
            ((ICollection<KeyValuePair<TKey, IList<TValue>>>)this.dictionary).CopyTo(array, arrayIndex);
        }

        bool ICollection<KeyValuePair<TKey, IList<TValue>>>.IsReadOnly
        {
            get { return ((ICollection<KeyValuePair<TKey, IList<TValue>>>)this.dictionary).IsReadOnly; }
        }

        bool ICollection<KeyValuePair<TKey, IList<TValue>>>.Remove(KeyValuePair<TKey, IList<TValue>> item)
        {
            return ((ICollection<KeyValuePair<TKey, IList<TValue>>>)this.dictionary).Remove(item);
        }

        #endregion

        #region IEnumerable<KeyValuePair<TKey,IList<TValue>>> Members

        IEnumerator<KeyValuePair<TKey, IList<TValue>>> IEnumerable<KeyValuePair<TKey, IList<TValue>>>.GetEnumerator()
        {
            return ((IEnumerable<KeyValuePair<TKey, IList<TValue>>>)this.dictionary).GetEnumerator();
        }

        #endregion

        #region IEnumerable Members

        IEnumerator IEnumerable.GetEnumerator()
        {
            return ((IEnumerable)this.dictionary).GetEnumerator();
        }

        #endregion

        #region Private Fields

        private readonly Dictionary<TKey, IList<TValue>> dictionary;

        #endregion
    }
}
