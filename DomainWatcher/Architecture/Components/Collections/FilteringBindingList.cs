﻿namespace DomainWatcher.Architecture.Components.Collections
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.ComponentModel;
    using System.Collections;

    public class FilteringBindingList<T> : BindingList<T>
    {
        #region Constructors

        public FilteringBindingList(BindingList<T> bindingList)
            : this(bindingList, x => true)
        {

        }

        public FilteringBindingList(BindingList<T> bindingList, Predicate<T> predicate)
        {
            this.predicate = predicate;
            this.bindingList = bindingList;

            this.Initialize();
        }

        #endregion

        #region Methods

        private void Initialize()
        {
            foreach (T item in this.bindingList.ToArray())
            {
                if (this.predicate(item))
                {
                    this.Add(item);
                }
            }
            this.bindingList.ListChanged += BindingList_ListChanged;
        }

        private void BindingList_ListChanged(object sender, ListChangedEventArgs e)
        {
            if (e.ListChangedType == ListChangedType.ItemAdded)
            {
                T item = this.bindingList[e.NewIndex];
                if (this.predicate(item))
                {
                    this.Add(item);
                }
            }
            else if (e.ListChangedType == ListChangedType.ItemChanged)
            {
                T item = this.bindingList[e.NewIndex];
                this[e.NewIndex] = item;
            }
            else if (e.ListChangedType == ListChangedType.ItemDeleted)
            {
                this.RemoveAt(e.NewIndex);
            }
            else if (e.ListChangedType == ListChangedType.ItemMoved)
            {
                T item = this[e.OldIndex];
                this.RemoveAt(e.OldIndex);
                this.Insert(e.NewIndex, item);
            }
            else if (e.ListChangedType == ListChangedType.Reset)
            {
                this.OnListChanged(e);
            }
        }

        #endregion

        #region Fields

        private readonly Predicate<T> predicate;
        private readonly BindingList<T> bindingList;

        #endregion
    }
}
