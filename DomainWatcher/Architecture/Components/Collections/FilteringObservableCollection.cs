﻿namespace DomainWatcher.Architecture.Components.Collections
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Collections.Specialized;
    using System.Collections.ObjectModel;
    using System.Collections;

    public class FilteringObservableCollection<T> : IEnumerable<T>, INotifyCollectionChanged
    {
        #region Public Properties

        public Predicate<T> FilterPredicate
        {
            get { return this.filterPredicate; }
            set
            {
                if (this.filterPredicate != value)
                {
                    this.filterPredicate = value;
                    this.filteredItems.Sort(this.sortComparison);
                }
            }
        }

        public Comparison<T> SortComparison
        {
            get { return this.sortComparison; }
            set
            {
                if (this.sortComparison != value)
                {
                    this.sortComparison = value;
                    this.filteredItems.Sort(value);
                }
            }
        }

        #endregion

        #region Constructors

        public FilteringObservableCollection(ObservableCollection<T> observableCollection)
            : this(observableCollection, x => true)
        {
        }

        public FilteringObservableCollection(ObservableCollection<T> observableCollection, Predicate<T> filterPredicate)
        {
            this.filteredItems = new ObservableList<T>();
            this.filterPredicate = filterPredicate;
            this.observableCollection = observableCollection;

            this.Initialize();
        }

        #endregion

        #region IEnumerable<T> Members

        public IEnumerator<T> GetEnumerator()
        {
            return this.filteredItems.Where(x => this.filterPredicate(x)).GetEnumerator();
        }

        #endregion

        #region IEnumerable Members

        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.filteredItems.Where(x => this.filterPredicate(x)).GetEnumerator();
        }

        #endregion

        #region INotifyCollectionChanged Members

        public event NotifyCollectionChangedEventHandler CollectionChanged
        {
            add { this.filteredItems.CollectionChanged += value; }
            remove { this.filteredItems.CollectionChanged -= value; }
        }

        #endregion

        #region Private Methods

        private void Initialize()
        {
            this.filteredItems.AddRange(this.observableCollection);

            this.observableCollection.CollectionChanged += this.ObservableCollectionCollectionChanged;
        }

        protected void ObservableCollectionCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            ObservableCollection<T> collection = (ObservableCollection<T>)(sender);
            if (e.Action == NotifyCollectionChangedAction.Add)
            {
                foreach (T addedItem in e.NewItems)
                {
                    this.filteredItems.Add(addedItem);
                }
            }
            else if (e.Action == NotifyCollectionChangedAction.Move)
            {
                // Not sure what to do in this case
            }
            else if (e.Action == NotifyCollectionChangedAction.Remove)
            {
                foreach (T removedItem in e.OldItems)
                {
                    this.filteredItems.Remove(removedItem);
                }
            }
            else if (e.Action == NotifyCollectionChangedAction.Replace)
            {
                this.filteredItems[e.NewStartingIndex] = (T)e.NewItems[0];
            }
            else if (e.Action == NotifyCollectionChangedAction.Reset)
            {
                if (!collection.Any())
                {
                    this.filteredItems.Clear();
                }
                else
                {
                    this.filteredItems.Sort(this.sortComparison);
                }
            }
        }

        #endregion

        #region Private Fields

        private Predicate<T> filterPredicate;
        private Comparison<T> sortComparison;

        private readonly ObservableList<T> filteredItems;
        private readonly ObservableCollection<T> observableCollection;

        #endregion
    }
}
