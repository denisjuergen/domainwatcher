﻿namespace DomainWatcher.Architecture.Components.Collections
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Collections.ObjectModel;
    using System.Collections.Specialized;

    public class ObservableList<T> : ObservableCollection<T>
    {
        #region Public Methods

        public void AddRange(IEnumerable<T> collection)
        {
            List<T> list = collection.ToList();
            this.SuppressEvents(list.Count);
            list.ForEach(i => this.Items.Add(i));
            this.OnCollectionChanged(list);
        }

        public void Sort(Comparison<T> comparison)
        {
            IEnumerable<T> items = this.OrderBy(i => i, new ListComparer(comparison)).ToArray();
            this.Items.Clear();
            this.SuppressSingleEvent();
            this.AddRange(items);
            this.OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
        }

        #endregion

        #region Protected Methods

        protected override void OnCollectionChanged(NotifyCollectionChangedEventArgs e)
        {
            if (!this.IsEventSuppressed())
            {
                base.OnCollectionChanged(e);
            }
        }

        protected void SuppressSingleEvent()
        {
            this.SuppressEvents(1);
        }

        protected void SuppressEvents(int supressCount)
        {
            this.eventsToSuppress += supressCount;
        }

        #endregion

        #region Private Methods

        private bool IsEventSuppressed()
        {
            return this.eventsToSuppress-- > 0;
        }

        private void OnCollectionChanged(IList<T> addedItems)
        {
            this.OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, addedItems));
        }

        #endregion

        #region Private Fields

        private int eventsToSuppress = 0;

        #endregion

        private class ListComparer : IComparer<T>
        {
            public ListComparer(Comparison<T> comparison)
            {
                this.comparison = comparison;
            }

            public int Compare(T x, T y)
            {
                return this.comparison(x, y);
            }

            private Comparison<T> comparison;
        }
    }
}