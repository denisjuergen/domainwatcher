﻿namespace DomainWatcher.Architecture.Components.Collections
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Collections.ObjectModel;
    using System.Collections.Specialized;
    using System.Collections;

    public class NotificationList<T> : IList<T>, INotifyCollectionChanged
    {
        #region Constructors

        public NotificationList()
        {
            this.items = new List<T>();
        }

        public NotificationList(IEnumerable<T> collection)
        {
            this.items = new List<T>(collection);
        }

        #endregion

        #region Public Methods

        public virtual void AddRange(IEnumerable<T> collection)
        {
            int index = this.items.Count;
            this.items.AddRange(collection);
            this.OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, collection.ToList(), index));
        }

        public virtual void Sort(Comparison<T> comparison)
        {
            this.items.Sort(comparison);
            this.OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
        }

        #endregion

        #region IList<T> Members

        public int IndexOf(T item)
        {
            return this.items.IndexOf(item);
        }

        public void Insert(int index, T item)
        {
            this.items.Insert(index, item);
            this.OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, item, index));
        }

        public void RemoveAt(int index)
        {
            var oldItem = this.items.ElementAtOrDefault(index);
            if (oldItem != null)
            {
                this.items.RemoveAt(index);
                this.OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Remove, oldItem, index));
            }
        }

        public T this[int index]
        {
            get { return this.items[index]; }
            set
            {
                bool add = this.items.ElementAtOrDefault(index) == null;
                this.items[index] = value;
                if (add)
                {
                    this.OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, value, index));
                }
                else
                {
                    this.OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Replace, value, index));
                }
            }
        }

        #endregion

        #region ICollection<T> Members

        public virtual void Add(T item)
        {
            this.items.Add(item);
            this.OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, item, this.items.LastIndexOf(item)));
        }

        public void Clear()
        {
            var oldItems = this.items.ToList();
            this.items.Clear();
            this.OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Remove, oldItems));
        }

        public bool Contains(T item)
        {
            return this.items.Contains(item);
        }

        public void CopyTo(T[] array, int arrayIndex)
        {
            this.items.CopyTo(array, arrayIndex);
        }

        public int Count
        {
            get { return this.items.Count; }
        }

        public bool IsReadOnly
        {
            get { return (this.items as IList<T>).IsReadOnly; }
        }

        public bool Remove(T item)
        {
            int index = this.items.IndexOf(item);
            if (this.items.Remove(item))
            {
                this.OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Remove, item, index));
            }
            return false;
        }

        #endregion

        #region IEnumerable<T> Members

        public IEnumerator<T> GetEnumerator()
        {
            return this.items.GetEnumerator();
        }

        #endregion

        #region IEnumerable Members

        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.items.GetEnumerator();
        }

        #endregion

        #region INotifyCollectionChanged Members

        public event NotifyCollectionChangedEventHandler CollectionChanged;

        #endregion

        #region Private Methods

        protected void OnCollectionChanged(NotifyCollectionChangedEventArgs e)
        {
            if (this.CollectionChanged != null)
            {
                this.CollectionChanged(this, e);
            }
        }

        #endregion

        #region Private Fields

        private List<T> items;

        #endregion
    }
}
