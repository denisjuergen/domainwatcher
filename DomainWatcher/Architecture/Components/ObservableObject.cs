﻿namespace DomainWatcher.Architecture.Components
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Text;
    using DomainWatcher.Architecture.Contracts;
    using DomainWatcher.Infrastructure;

    public abstract class ObservableObject : INotifyPropertyChanged
    {
        #region Protected Members

        protected virtual void RaisePropertyChanged<T>(Expression<Func<T>> expression)
        {
            var propertyName = PropertySupport.ExtractPropertyName(expression);
            this.RaisePropertyChanged(propertyName);
        }

        protected virtual void RaisePropertyChanged(string propertyName)
        {
            var e = new PropertyChangedEventArgs(propertyName);
            this.RaisePropertyChanged(e);
        }

        #endregion

        #region INotifyPropertyChanged Members

        public virtual event PropertyChangedEventHandler PropertyChanged;

        #endregion

        #region Private Methods

        private void RaisePropertyChanged(PropertyChangedEventArgs e)
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, e);
            }
        }

        #endregion
    }
}
