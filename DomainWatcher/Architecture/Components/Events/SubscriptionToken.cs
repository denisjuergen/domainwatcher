﻿namespace DomainWatcher.Architecture.Components.Events
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    public sealed class SubscriptionToken
    {
        public Guid ID { get; private set; }

        public SubscriptionToken()
        {
            this.ID = Guid.NewGuid();
        }
    }
}
