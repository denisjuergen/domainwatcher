﻿namespace DomainWatcher.Architecture.Components.Events
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    public class EventPayload
    {
        public static EventPayload Empty = new EventPayload();

        public bool CancelFurtherPropagation { get; set; }

        protected EventPayload()
        {
        }
    }
}
