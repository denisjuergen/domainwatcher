﻿namespace DomainWatcher.Architecture.Components.Events
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using DomainWatcher.Architecture.Extensions;

    public sealed class EventAggregator : IEventAggregator
    {
        #region Constructors

        public EventAggregator()
        {
            this.events = new Dictionary<Type, ICompositeEvent>();
        }

        #endregion

        #region IEventAggregator Members

        public TEvent GetEvent<TEvent>() where TEvent : class, ICompositeEvent, new()
        {
            ICompositeEvent evt = null;
            if (!events.TryGetValue(typeof(TEvent), out evt))
            {
                evt = new TEvent();
                events[typeof(TEvent)] = evt;
            }
            return (TEvent)(evt);
        }

        #endregion

        #region Private Fields

        private readonly Dictionary<Type, ICompositeEvent> events;

        #endregion
    }
}
