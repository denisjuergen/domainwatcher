﻿namespace DomainWatcher.Architecture.Components.Events
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    public abstract class CompositeEvent<TPayload> : ICompositeEvent where TPayload : EventPayload
    {
        #region Constructors

        public CompositeEvent()
        {
            this.subscribers = new Dictionary<SubscriptionToken, Action<TPayload>>();
        }

        #endregion

        #region Public Methods

        public SubscriptionToken Subscribe(Action<TPayload> eventCallback)
        {
            SubscriptionToken subscriptionToken = new SubscriptionToken();
            this.subscribers[subscriptionToken] = eventCallback;
            return subscriptionToken;
        }

        #endregion

        #region ICompositeEvent Members

        SubscriptionToken ICompositeEvent.Subscribe(Action<object> eventCallback)
        {
            return this.Subscribe(p => eventCallback(p));
        }

        public void UnSubscribe(SubscriptionToken subscriptionToken)
        {
            this.subscribers.Remove(subscriptionToken);
        }

        #endregion

        #region Internal Methods

        internal void Publish(TPayload payload)
        {
            var subscribers = this.subscribers.Values.ToArray();
            foreach (var subscriber in subscribers)
            {
                subscriber(payload);
                if (payload.CancelFurtherPropagation)
                {
                    break;
                }
            }
        }

        #endregion

        #region Private Fields

        private readonly Dictionary<SubscriptionToken, Action<TPayload>> subscribers;

        #endregion
    }
}
