﻿namespace DomainWatcher.Architecture.Components.Events
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    public interface IEventAggregator
    {
        TEvent GetEvent<TEvent>() where TEvent : class, ICompositeEvent, new();
    }
}
