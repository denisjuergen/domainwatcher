﻿namespace DomainWatcher.Architecture.Components.Events
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    public sealed class EventLocator<TEvent> where TEvent : class, ICompositeEvent, new()
    {
        public TEvent Event { get { return this.eventAggregator.GetEvent<TEvent>(); } }

        public EventLocator(IEventAggregator eventAggregator)
        {
            this.eventAggregator = eventAggregator;
        }

        private readonly IEventAggregator eventAggregator;
    }
}
