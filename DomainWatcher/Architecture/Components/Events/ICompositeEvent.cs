﻿namespace DomainWatcher.Architecture.Components.Events
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    public interface ICompositeEvent
    {
        SubscriptionToken Subscribe(Action<object> eventCallback);
        void UnSubscribe(SubscriptionToken subscriptionToken);
    }
}
