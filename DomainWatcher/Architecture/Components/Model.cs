﻿namespace DomainWatcher.Architecture.Components
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using DomainWatcher.Architecture.Contracts;

    public abstract class Model : ObservableObject, IModel
    {
        #region Constructors

        public Model()
        {
            this.Validator = new Validator<Model>(this);
        }

        #endregion

        #region Properties

        protected IValidator<Model> Validator { get; private set; }

        #endregion

        #region IModel Members

        public bool IsValid
        {
            get
            {
                this.Validate();
                return this.Validator.ErrorCount == 0;
            }
        }

        public abstract void Validate();

        #endregion

        #region IDataErrorInfo Members

        public string Error
        {
            get { return this.Validator.Error; }
        }

        public string this[string columnName]
        {
            get { return this.Validator[columnName]; }
        }

        #endregion
    }
}
