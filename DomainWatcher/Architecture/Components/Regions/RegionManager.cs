﻿namespace DomainWatcher.Architecture.Components.Regions
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using DomainWatcher.Infrastructure;

    public class RegionManager : IRegionManager
    {
        #region Constructors

        public RegionManager(IRegionAdapterLocator regionAdapterLocator)
        {
            this.regions = new HashSet<IRegion>();
            this.regionAdapterLocator = regionAdapterLocator;
        }

        #endregion

        #region IRegionManager Members

        public IEnumerable<IRegion> Regions
        {
            get { return this.regions.ToArray(); }
        }

        public IRegion RegisterRegion(object regionTarget, string regionName)
        {
            ValidationSupport.ThrowIfArgumentNull(() => regionTarget);

            Type regionTargetType = regionTarget.GetType();
            if (this.regionAdapterLocator.HasAdapter(regionTargetType))
            {
                IRegionAdapter regionAdapter = this.regionAdapterLocator.GetAdapter(regionTargetType);
                IRegion region = regionAdapter.Adapt(regionTarget, regionName);
                this.regions.Add(region);

                return region;
            }

            return null;
        }

        #endregion

        #region Private Fields

        private readonly HashSet<IRegion> regions;
        private readonly IRegionAdapterLocator regionAdapterLocator;

        #endregion
    }
}
