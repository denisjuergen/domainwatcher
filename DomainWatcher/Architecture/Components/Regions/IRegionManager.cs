﻿namespace DomainWatcher.Architecture.Components.Regions
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    public interface IRegionManager
    {
        IEnumerable<IRegion> Regions { get; }

        IRegion RegisterRegion(object regionTarget, string regionName);
    }
}
