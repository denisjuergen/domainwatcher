﻿namespace DomainWatcher.Architecture.Components.Regions
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    public interface IRegionViewRegistry
    {
        IEnumerable<object> GetRegisteredRegionContents(string regionName);
        void RegisterViewWithRegion(string regionName, Func<object> getViewDelegate);
    }
}
