﻿namespace DomainWatcher.Architecture.Components.Regions
{
    using System.Collections.ObjectModel;

    public interface IRegion
    {
        string Name { get; set; }
        ReadOnlyObservableCollection<object> Views { get; }

        void Add(object view, string viewName);
        void Remove(object view);
        object GetView(string viewName);
    }
}
