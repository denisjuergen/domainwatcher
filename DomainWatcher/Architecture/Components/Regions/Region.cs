﻿namespace DomainWatcher.Architecture.Components.Regions
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Collections.ObjectModel;
    using System.Globalization;
    using DomainWatcher.Properties;

    public class Region : IRegion
    {
        #region Constructors

        public Region()
        {
            this.views = new ObservableCollection<object>();
            this.readOnlyViews = new ReadOnlyObservableCollection<object>(this.views);
            this.metaDataDictionary = new Dictionary<object, ViewMetaData>();
        }

        #endregion

        #region IRegion Members

        public string Name
        {
            get;
            set;
        }

        public ReadOnlyObservableCollection<object> Views
        {
            get { return this.readOnlyViews; }
        }

        public void Add(object view, string viewName)
        {
            if (!this.views.Contains(view))
            {
                var metaData = new ViewMetaData
                {
                    Name = viewName
                };
                this.views.Add(view);
                this.metaDataDictionary[view] = metaData;
            }
        }

        public void Remove(object view)
        {
            this.views.Remove(view);
            this.metaDataDictionary.Remove(view);
        }

        public object GetView(string viewName)
        {
            var entry = this.metaDataDictionary.SingleOrDefault(x => x.Value.Name.Equals(viewName));
            if (entry.Value == null || entry.Key == null)
            {
                throw new KeyNotFoundException(string.Format(CultureInfo.CurrentCulture, Resources.RegionViewNotFoundException, viewName));
            }
            return entry.Key;
        }

        #endregion

        #region Private Fields

        private readonly ObservableCollection<object> views;
        private readonly ReadOnlyObservableCollection<object> readOnlyViews;
        private readonly Dictionary<object, ViewMetaData> metaDataDictionary;

        #endregion
    }
}
