﻿namespace DomainWatcher.Architecture.Components.Regions
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    public interface IRegionAdapter
    {
        IRegion Adapt(object regionTarget, string regionName);
    }
}
