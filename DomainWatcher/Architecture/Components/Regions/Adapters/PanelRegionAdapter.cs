﻿namespace DomainWatcher.Architecture.Components.Regions.Adapters
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Windows.Forms;
    using System.Collections.Specialized;

    public class PanelRegionAdapter : RegionAdapterBase<Panel>
    {
        #region Constructors

        public PanelRegionAdapter(IRegionViewRegistry regionViewRegistry)
        {
            this.regionViewRegistry = regionViewRegistry;
        }

        #endregion

        #region RegionAdapterBase<Panel> Members

        protected override IRegion Adapt(IRegion region, Panel regionTarget)
        {
            var views = region.Views as INotifyCollectionChanged;

            views.CollectionChanged += (s, e) =>
            {
                Control control = null;
                switch (e.Action)
                {
                    case NotifyCollectionChangedAction.Add:
                        control = e.NewItems.OfType<Control>().FirstOrDefault();
                        this.TryAddControl(regionTarget, control);
                        break;
                    case NotifyCollectionChangedAction.Remove:
                        control = e.OldItems.OfType<Control>().FirstOrDefault();
                        if (regionTarget.Controls.Contains(control))
                        {
                            regionTarget.Controls.Remove(control);
                            control = region.Views.OfType<Control>().FirstOrDefault();
                            this.TryAddControl(regionTarget, control);
                        }
                        break;
                    default:
                        regionTarget.Controls.Clear();
                        control = region.Views.OfType<Control>().FirstOrDefault() ?? e.NewItems.OfType<Control>().FirstOrDefault();
                        this.TryAddControl(regionTarget, control);
                        break;
                }
            };

            regionTarget.Layout += (s, e) =>
            {
                foreach (var view in this.regionViewRegistry.GetRegisteredRegionContents(region.Name))
                {
                    region.Add(view, null);
                }
            };

            regionTarget.ControlAdded += (s, e) =>
            {
                e.Control.Dock = DockStyle.Fill;
            };

            return region;
        }

        #endregion

        #region Private Methods

        private void TryAddControl(Panel panel, Control control)
        {
            if (panel.Controls.Count == 0 && control != null)
            {
                panel.Controls.Add(control);
            }
        }

        #endregion

        #region Private Fields

        private readonly IRegionViewRegistry regionViewRegistry;

        #endregion
    }
}
