﻿namespace DomainWatcher.Architecture.Components.Regions
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    public class ViewMetaData
    {
        public string Name { get; set; }
    }
}
