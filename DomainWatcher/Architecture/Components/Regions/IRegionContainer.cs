﻿namespace DomainWatcher.Architecture.Components.Regions
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Windows.Forms;
    using DomainWatcher.Architecture.Contracts;

    public interface IRegionContainer
    {
        string Name { get; }

        event EventHandler Disposed;
        event EventHandler Load;
    }
}
