﻿namespace DomainWatcher.Architecture.Components.Regions
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using DomainWatcher.Infrastructure;
    using DomainWatcher.Architecture.Components.Collections;

    public class RegionViewRegistry : IRegionViewRegistry
    {
        #region Constructors

        public RegionViewRegistry()
        {
            this.regionRegisteredViews = new ListDictionary<string, Func<object>>();
        }

        #endregion

        #region IViewRegistry Members

        public IEnumerable<object> GetRegisteredRegionContents(string regionName)
        {
            ValidationSupport.ThrowIfArgumentNull(() => regionName);
            foreach (var getViewObjectDelegate in this.regionRegisteredViews[regionName])
            {
                yield return getViewObjectDelegate();
            }
        }

        public void RegisterViewWithRegion(string regionName, Func<object> getViewDelegate)
        {
            ValidationSupport.ThrowIfArgumentNull(() => regionName);
            ValidationSupport.ThrowIfArgumentNull(() => getViewDelegate);
            this.regionRegisteredViews.Add(regionName, getViewDelegate);
        }

        #endregion

        #region Private Fields

        private readonly ListDictionary<string, Func<object>> regionRegisteredViews;

        #endregion
    }
}
