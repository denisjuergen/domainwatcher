﻿namespace DomainWatcher.Architecture.Components.Regions
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    public interface IRegionAdapterLocator
    {
        bool HasAdapter(Type targetType);
        IRegionAdapter GetAdapter(Type targetType);
        void RegisterAdapter(Type targetType, IRegionAdapter regionAdapter);
    }
}
