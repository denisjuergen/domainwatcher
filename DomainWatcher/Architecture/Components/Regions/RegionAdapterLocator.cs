﻿namespace DomainWatcher.Architecture.Components.Regions
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using DomainWatcher.Infrastructure;
    using System.Globalization;
    using DomainWatcher.Properties;

    public class RegionAdapterLocator : IRegionAdapterLocator
    {
        #region Constructors

        public RegionAdapterLocator()
        {
            this.mappings = new Dictionary<Type, IRegionAdapter>();
        }

        #endregion

        #region IRegionAdapterLocator Members

        public bool HasAdapter(Type targetType)
        {
            return this.mappings.ContainsKey(targetType);
        }

        public IRegionAdapter GetAdapter(Type targetType)
        {
            ValidationSupport.ThrowIfArgumentNull(() => targetType);

            Type currentType = targetType;
            while (currentType != null)
            {
                if (this.mappings.ContainsKey(currentType))
                {
                    return this.mappings[currentType];
                }

                currentType = currentType.BaseType;
            }

            throw new KeyNotFoundException(string.Format(CultureInfo.CurrentCulture, Resources.RegionAdapterNotFoundException, targetType));
        }

        public void RegisterAdapter(Type targetType, IRegionAdapter regionAdapter)
        {
            ValidationSupport.ThrowIfArgumentNull(() => targetType);
            ValidationSupport.ThrowIfArgumentNull(() => regionAdapter);

            this.mappings[targetType] = regionAdapter;
        }

        #endregion

        #region Private Fields

        private readonly Dictionary<Type, IRegionAdapter> mappings;

        #endregion
    }
}
