﻿namespace DomainWatcher.Architecture.Components.Regions
{
    using System;
    using System.Globalization;
    using DomainWatcher.Architecture.Extensions;
    using DomainWatcher.Infrastructure;
    using DomainWatcher.Properties;

    public abstract class RegionAdapterBase<T> : IRegionAdapter where T : class
    {
        #region Abstract Members

        protected abstract IRegion Adapt(IRegion region, T regionTarget);

        protected virtual T GetCastedObject(object regionTarget)
        {
            ValidationSupport.ThrowIfArgumentNull(() => regionTarget);

            T castedTarget = regionTarget as T;
            if (castedTarget == null)
            {
                throw new InvalidOperationException(string.Format(CultureInfo.CurrentCulture, Resources.RegionAdapterNotSupportedTypeException, regionTarget.GetType().Name));
            }

            return castedTarget;
        }

        #endregion

        #region IRegionAdapter Members

        public IRegion Adapt(object regionTarget, string regionName)
        {
            IRegion region = ServiceLocator.Current.GetInstance<IRegion>();

            region.Name = regionName;

            return this.Adapt(region, this.GetCastedObject(regionTarget));
        }

        #endregion
    }
}
