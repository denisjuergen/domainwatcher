﻿namespace DomainWatcher.Architecture.Components
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Windows.Forms;
    using DomainWatcher.Architecture.Contracts;
    using DomainWatcher.Infrastructure;
    using DomainWatcher.Architecture.Components.DataBinding;

    public abstract class Presenter<TView, TPresenter> : IPresenter<TView, TPresenter>
        where TView : IView<TPresenter, TView>
        where TPresenter : IPresenter<TView, TPresenter>
    {
        #region Constructors

        public Presenter()
        {
            this.views = new HashSet<TView>();
            this.PropertyChangedTracker = new PropertyChangedTracker();
        }

        #endregion

        #region Properties

        protected PropertyChangedTracker PropertyChangedTracker { get; private set; }

        #endregion

        #region IPresenter<TView,TPresenter> Members

        public IEnumerable<TView> Views
        {
            get { return this.views; }
        }

        public virtual void AttachView(TView view)
        {
            ValidationSupport.ThrowIfArgumentNull(() => view);

            if (this.views.Add(view))
            {
                view.Disposed += (s, e) => this.DetachView((TView)s);
                view.Initialize();
            }
        }

        public virtual void DetachView(TView view)
        {
            ValidationSupport.ThrowIfArgumentNull(() => view);

            if (this.views.Remove(view))
            {
                view.Disposed -= (s, e) => this.DetachView((TView)s);
            }
        }

        #endregion

        #region Private Fields

        private HashSet<TView> views;

        #endregion
    }
}
