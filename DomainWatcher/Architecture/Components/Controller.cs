﻿namespace DomainWatcher.Architecture.Components
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Windows.Forms;
    using DomainWatcher.Architecture.Contracts;

    public abstract class Controller<TView, TPresenter> : IController<TView, TPresenter>
        where TView : IView<TPresenter, TView>
        where TPresenter : IPresenter<TView, TPresenter>
    {
        #region Constructors

        public Controller(TPresenter presenter, IViewFactory<TView, TPresenter> viewFactory)
        {
            this.presenter = presenter;
            this.viewFactory = viewFactory;
        }

        #endregion

        #region IController<TView,TPresenter> Members

        public IEnumerable<TView> Views
        {
            get { return this.presenter.Views; }
        }

        public TPresenter Presenter
        {
            get { return this.presenter; }
        }

        public TView CreateView()
        {
            TView view = this.viewFactory.CreateView();
            this.presenter.AttachView(view);
            return view;
        }

        public bool DestroyView(TView view)
        {
            if (this.presenter.Views.Contains(view))
            {
                this.presenter.DetachView(view);
                view.Dispose();
                return true;
            }
            return false;
        }

        #endregion

        #region Private Fields

        private readonly TPresenter presenter;
        private readonly IViewFactory<TView, TPresenter> viewFactory;

        #endregion
    }
}
