﻿namespace DomainWatcher.Architecture.Components
{
    using System.Windows.Forms;
    using DomainWatcher.Architecture.Components.Logging;
    using DomainWatcher.Architecture.Components.Modules;
    using DomainWatcher.Architecture.Components.Regions;
    using DomainWatcher.Architecture.Contracts;
    using DomainWatcher.Architecture.Extensions;
    using DomainWatcher.Architecture.Components.Regions.Adapters;

    public abstract class Boostrapper
    {
        #region Public Properties

        public Form Shell { get; private set; }
        public ILogger Logger { get; private set; }
        public IRegionManager RegionManager { get; private set; }
        public IModuleManager ModuleManager { get; private set; }

        #endregion

        #region Public Methods

        public virtual void Run()
        {
            this.SetupServiceLocator();
            this.SetupLogger();
            this.SetupRegionManager();
            this.SetupModules();
            this.LaunchApplicationShell();
        }

        #endregion

        #region Protected Methods

        protected virtual ILogger CreateLogger()
        {
            return ServiceLocator.Current.GetInstance<ILogger>();
        }

        protected abstract IServiceLocator CreateServiceLocator();

        protected virtual void ConfigureRegionAdapterLocator(IRegionAdapterLocator regionAdapterLocator)
        {
            regionAdapterLocator.RegisterAdapter(typeof(Panel), ServiceLocator.Current.GetInstance<PanelRegionAdapter>());
        }

        protected virtual void ConfigureRegionManager(IRegionManager regionManager)
        {
        }

        protected virtual void ConfigureModuleCatalog(IModuleCatalog moduleCatalog)
        {
        }

        protected abstract Form CreateShell();

        #endregion

        #region Private Methods

        private void SetupServiceLocator()
        {
            ServiceLocator.SetServiceLocator(this.CreateServiceLocator());
        }

        private void SetupLogger()
        {
            this.Logger = this.CreateLogger();
            this.Logger.Log("Logger setup complete", LogLevel.Debug);
        }

        private void SetupRegionManager()
        {
            var adapterLocator = ServiceLocator.Current.GetInstance<IRegionAdapterLocator>();
            this.ConfigureRegionAdapterLocator(adapterLocator);
            this.Logger.Log("RegionAdapterLocator configured", LogLevel.Debug);

            var regionManager = ServiceLocator.Current.GetInstance<IRegionManager>();
            this.ConfigureRegionManager(regionManager);
            this.RegionManager = regionManager;
            this.Logger.Log("RegionManager setup complete", LogLevel.Debug);
        }

        private void SetupModules()
        {
            var moduleCatalog = ServiceLocator.Current.GetInstance<IModuleCatalog>();
            this.ConfigureModuleCatalog(moduleCatalog);
            this.Logger.Log("ModuleCatalog created and configured", LogLevel.Debug);

            this.ModuleManager = ServiceLocator.Current.GetInstance<IModuleManager>();
            this.Logger.Log("ModuleManager setup complete", LogLevel.Debug);
            this.Logger.Log("Initializing modules...", LogLevel.Debug);
            this.ModuleManager.Bootstrap();
        }

        private void LaunchApplicationShell()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            this.Shell = this.CreateShell();
            this.Logger.Log("Application shell created", LogLevel.Debug);
            this.Logger.Log("Launching application shell...", LogLevel.Debug);


            Application.Run(this.Shell);
        }

        #endregion
    }
}
