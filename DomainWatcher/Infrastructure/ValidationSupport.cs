﻿namespace DomainWatcher.Infrastructure
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Linq.Expressions;

    public static class ValidationSupport
    {
        public static void ThrowArgumentException<T>(Expression<Func<T>> expression, string message)
        {
            throw new ArgumentException(message, ValidationSupport.ExtractFieldName(expression));
        }

        public static void ThrowIfArgumentNull<T>(Expression<Func<T>> expression)
        {
            ValidationSupport.ThrowIfArgumentNull(
                expression,
                ValidationSupport.ExtractFieldName(() => expression));

            ValidationSupport.ThrowIfArgumentNull(
                expression.Compile().Invoke(),
                ValidationSupport.ExtractFieldName(expression));
        }

        public static void ThrowIfArgumentOutOfRange<T>(Expression<Func<T>> expression, IEnumerable<T> collection)
        {
            if (!collection.Contains(expression.Compile().Invoke()))
            {
                throw new ArgumentOutOfRangeException(ValidationSupport.ExtractFieldName(expression));
            }
        }

        private static void ThrowIfArgumentNull(object argument, string argumentName)
        {
            if (object.ReferenceEquals(null, argument))
            {
                throw new ArgumentNullException(argumentName);
            }
        }

        private static string ExtractFieldName<T>(Expression<Func<T>> expression)
        {
            MemberExpression memberExpression = expression.Body as MemberExpression;
            if (memberExpression == null)
            {
                throw new ArgumentException(string.Format(
                    "Expression '{0}' refers to a method, not a field.",
                    expression.ToString()));
            }
            return memberExpression.Member.Name;
        }
    }
}
