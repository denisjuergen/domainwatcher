﻿namespace DomainWatcher.Infrastructure
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Reflection;
    using System.Text;

    public static class PropertySupport
    {
        public static string ExtractPropertyName<T>(Expression<Func<T>> propertyExpression)
        {
            ValidationSupport.ThrowIfArgumentNull(() => propertyExpression);

            MemberExpression memberExpression = propertyExpression.Body as MemberExpression;
            if (memberExpression == null)
            {
                throw new ArgumentException(string.Format(
                    "Expression '{0}' refers to a method, not a property.",
                    propertyExpression.ToString()));
            }

            PropertyInfo propertyInfo = memberExpression.Member as PropertyInfo;
            if (propertyInfo == null)
            {
                throw new ArgumentException(string.Format(
                    "Expression '{0}' refers to a field, not a property.",
                    propertyExpression.ToString()));
            }

            if (propertyInfo.GetGetMethod(true).IsStatic)
            {
                throw new ArgumentException(string.Format(
                    "Expressions '{0}' refers to a static property, not a class member.",
                    propertyExpression.ToString()));
            }

            return memberExpression.Member.Name;
        }

        public static string ExtractPropertyName<T1, T2>(Expression<Func<T1, T2>> propertyExpression)
        {
            ValidationSupport.ThrowIfArgumentNull(() => propertyExpression);

            MemberExpression memberExpression = propertyExpression.Body as MemberExpression;
            if (memberExpression == null)
            {
                throw new ArgumentException(string.Format(
                    "Expression '{0}' refers to a method, not a property.",
                    propertyExpression.ToString()));
            }

            PropertyInfo propertyInfo = memberExpression.Member as PropertyInfo;
            if (propertyInfo == null)
            {
                throw new ArgumentException(string.Format(
                    "Expression '{0}' refers to a field, not a property.",
                    propertyExpression.ToString()));
            }

            if (propertyInfo.GetGetMethod(true).IsStatic)
            {
                throw new ArgumentException(string.Format(
                    "Expressions '{0}' refers to a static property, not a class member.",
                    propertyExpression.ToString()));
            }

            if (memberExpression.Member.ReflectedType == typeof(T1))
            {
                throw new ArgumentException(string.Format(
                    "Expression '{0}' does not refers to a member of an instance of the class '{1}'.",
                    propertyExpression.ToString(), typeof(T1).Name));
            }

            return memberExpression.Member.Name;
        }

        public static void ExchangePropertyData(object donor, string donorPropertyName, object receiver, string receiverPropertyName)
        {
            var donorProperty = donor.GetType().GetProperty(donorPropertyName);
            var receiverProperty = receiver.GetType().GetProperty(receiverPropertyName);
            var donatedValue = donorProperty.GetValue(donor, null);

            receiverProperty.SetValue(receiver, donatedValue, null);
        }
    }
}
