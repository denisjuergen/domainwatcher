﻿namespace DomainWatcher.Infrastructure
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    public static class RegionNames
    {
        public const string ShellContent = "Shell.Content";
        public const string ShellSidebar = "Shell.Sidebar";
        public const string ShellLoggingWindow = "Shell.LoggingWindow";
    }
}
