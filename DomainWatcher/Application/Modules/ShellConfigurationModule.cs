﻿namespace DomainWatcher.Application.Modules
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using DomainWatcher.Architecture.Components.Modules;
    using Microsoft.Practices.Unity;
    using DomainWatcher.Application.Events;
    using DomainWatcher.Architecture.Components.Events;

    public class ShellConfigurationModule : IModule
    {
        #region Properties

        [Dependency]
        public EventLocator<ApplicationStarted> ApplicationStarted { get; set; }

        public SubscriptionToken ApplicationStartedToken { get; private set; }

        #endregion

        #region Methods

        private void ConfigureShell(ApplicationStarted.Payload eventPayload)
        {
            eventPayload.Shell.Text = "DomainWatcher";
            eventPayload.Shell.MinimumSize = new System.Drawing.Size(854, 480);

            this.ApplicationStarted.Event.UnSubscribe(this.ApplicationStartedToken);
        }

        #endregion

        #region IModule Members

        public void Initialize()
        {
            this.ApplicationStartedToken = this.ApplicationStarted.Event.Subscribe(this.ConfigureShell);
        }

        #endregion
    }
}
