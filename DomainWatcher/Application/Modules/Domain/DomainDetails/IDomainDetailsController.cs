﻿namespace DomainWatcher.Application.Modules.Domain.DomainDetails
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using DomainWatcher.Architecture.Contracts;

    public interface IDomainDetailsController : IController<IDomainDetailsView, IDomainDetailsPresenter>
    {
    }
}
