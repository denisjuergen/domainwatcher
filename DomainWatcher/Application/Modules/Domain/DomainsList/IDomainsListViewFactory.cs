﻿namespace DomainWatcher.Application.Modules.Domain.DomainsList
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using DomainWatcher.Architecture.Contracts;

    public interface IDomainsListViewFactory : IViewFactory<IDomainsListView, IDomainsListPresenter>
    {
    }
}
