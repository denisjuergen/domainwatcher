﻿namespace DomainWatcher.Application.Modules.Logging
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using DomainWatcher.Architecture.Components;

    public sealed class LoggingViewFactory : ViewFactory<ILoggingView, ILoggingPresenter>, ILoggingViewFactory
    {
    }
}
