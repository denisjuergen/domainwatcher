﻿namespace DomainWatcher.Application.Modules.Logging
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using DomainWatcher.Architecture.Components.Logging;
    using System.Collections.ObjectModel;
    using System.ComponentModel;
    using DomainWatcher.Architecture.Components;

    public class LoggingModel : Model, ILoggingModel
    {
        #region Constructors

        public LoggingModel()
        {
            this.LogEntries = new BindingList<LogEntry>();
            this.MinimumLoggingLevel = LogLevel.Debug;
        }

        #endregion

        #region ILoggingModel Members

        public BindingList<LogEntry> LogEntries { get; private set; }

        public LogLevel MinimumLoggingLevel
        {
            get { return this.minLogLevel; }
            set
            {
                if (this.minLogLevel != value)
                {
                    this.minLogLevel = value;
                    this.RaisePropertyChanged(() => this.MinimumLoggingLevel);
                }
            }
        }

        public override void Validate()
        {
            this.Validator.Validate(x => x.IsValid, x => x == true, "LoggingModel is invalid");
        }

        #endregion

        #region Fields

        private LogLevel minLogLevel;

        #endregion
    }
}
