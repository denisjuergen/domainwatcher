﻿namespace DomainWatcher.Application.Modules.Logging
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using DomainWatcher.Architecture.Components;
    using DomainWatcher.Architecture.Extensions;
    using DomainWatcher.Architecture.Components.Logging;
    using DomainWatcher.Architecture.Components.DataBinding;
    using DomainWatcher.Architecture.Components.Collections;

    public sealed class LoggingPresenter : Presenter<ILoggingView, ILoggingPresenter>, ILoggingPresenter
    {
        #region Constructors

        public LoggingPresenter(ILoggingModel loggingModel)
        {
            this.loggingModel = loggingModel;

            this.CreateLogEntriesList(this.loggingModel.MinimumLoggingLevel);
            this.PropertyChangedTracker.TrackPropertyChange(loggingModel, x => x.MinimumLoggingLevel, this.CreateLogEntriesList);
        }

        #endregion

        #region Properties

        public FilteringBindingList<LogEntry> LogEntries { get; private set; }

        public LogLevel MinimumLoggingLevel
        {
            get { return this.loggingModel.MinimumLoggingLevel; }
            set { this.loggingModel.MinimumLoggingLevel = value; }
        }

        #endregion

        #region Methods

        public override void AttachView(ILoggingView view)
        {
            base.AttachView(view);
            view.LogEntries = this.LogEntries;
        }

        private void CreateLogEntriesList(LogLevel logLevel)
        {
            this.LogEntries = new FilteringBindingList<LogEntry>(this.loggingModel.LogEntries, x => x.Level >= logLevel);
            foreach (ILoggingView view in this.Views.ToArray())
            {
                view.LogEntries = this.LogEntries;
            }
        }

        #endregion

        #region Fields

        private readonly ILoggingModel loggingModel;

        #endregion
    }
}
