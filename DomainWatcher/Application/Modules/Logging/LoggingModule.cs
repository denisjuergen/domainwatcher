﻿namespace DomainWatcher.Application.Modules.Logging
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using DomainWatcher.Architecture.Components.Modules;
    using DomainWatcher.Architecture.Components.Regions;
    using Microsoft.Practices.Unity;
    using DomainWatcher.Infrastructure;
    using DomainWatcher.Architecture.Components;
    using DomainWatcher.Architecture.Extensions;
    using DomainWatcher.Architecture.Components.Logging;

    public sealed class LoggingModule : IModule
    {
        #region Constructors

        public LoggingModule(ILogger logger, IUnityContainer unityContainer, IRegionViewRegistry regionViewRegistry)
        {
            this.logger = logger;
            this.unityContainer = unityContainer;
            this.regionViewRegistry = regionViewRegistry;
        }

        #endregion

        #region IModule Members

        public void Initialize()
        {
            this.RegisterTypes();
            this.RegisterViews();
            this.DoInitialize();
        }

        #endregion

        #region Private Methods

        private void RegisterTypes()
        {
            this.unityContainer.RegisterType<ILoggingView, LoggingView>(new ContainerControlledLifetimeManager())
                               .RegisterType<ILoggingModel, LoggingModel>(new ContainerControlledLifetimeManager())
                               .RegisterType<ILoggingPresenter, LoggingPresenter>(new ContainerControlledLifetimeManager())
                               .RegisterType<ILoggingController, LoggingController>(new ContainerControlledLifetimeManager())
                               .RegisterType<ILoggingViewFactory, LoggingViewFactory>(new ContainerControlledLifetimeManager())
                               .RegisterType<ILoggingInterceptor, LoggingInterceptor>(new ContainerControlledLifetimeManager());
        }

        private void RegisterViews()
        {
            this.regionViewRegistry.RegisterViewWithRegion(RegionNames.ShellLoggingWindow, () => ServiceLocator.Current.GetInstance<ILoggingController>().GetFirstOrCreateView());
        }

        private void DoInitialize()
        {
            this.logger.Listeners.Add(this.unityContainer.Resolve<ILoggingInterceptor>());
        }

        #endregion

        #region Private Fields

        private readonly ILogger logger;
        private readonly IUnityContainer unityContainer;
        private readonly IRegionViewRegistry regionViewRegistry;

        #endregion
    }
}
