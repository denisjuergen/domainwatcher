﻿namespace DomainWatcher.Application.Modules.Logging
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using DomainWatcher.Architecture.Components.Logging;

    public class LoggingInterceptor : ILoggingInterceptor
    {
        #region Constructors

        public LoggingInterceptor(ILoggingModel loggingModel)
        {
            this.loggingModel = loggingModel;
        }

        #endregion

        #region ILogger Members

        public void Log(string message, LogLevel logLevel)
        {
            this.loggingModel.LogEntries.Add(LogEntry.Create(message, logLevel));
        }

        #endregion

        #region Private Fields

        private readonly ILoggingModel loggingModel;

        #endregion
    }
}
