﻿namespace DomainWatcher.Application.Modules.Logging
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using DomainWatcher.Architecture.Components;

    public sealed class LoggingController : Controller<ILoggingView, ILoggingPresenter>, ILoggingController
    {
        #region Constructors

        public LoggingController(ILoggingPresenter presenter, ILoggingViewFactory viewFactory)
            : base(presenter, viewFactory)
        {

        }

        #endregion
    }
}
