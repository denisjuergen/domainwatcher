﻿namespace DomainWatcher.Application.Modules.Logging
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using DomainWatcher.Architecture.Contracts;
    using System.Collections.ObjectModel;
    using System.Collections.Specialized;
    using DomainWatcher.Architecture.Components.Logging;
    using System.ComponentModel;

    public interface ILoggingModel : IModel
    {
        LogLevel MinimumLoggingLevel { get; set; }
        BindingList<LogEntry> LogEntries { get; }
    }
}
