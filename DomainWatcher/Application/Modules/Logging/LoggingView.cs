﻿namespace DomainWatcher.Application.Modules.Logging
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Drawing;
    using System.Data;
    using System.Linq;
    using System.Text;
    using System.Windows.Forms;

    public partial class LoggingView : UserControl, ILoggingView
    {
        #region Methods

        private void listBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            ListBox listBox = (ListBox)(sender);
            listBox.SelectedIndex = -1;
        }

        private void listBox_DrawItem(object sender, DrawItemEventArgs e)
        {
            if (e.Index == -1)
            {
                return;
            }

            Brush brush = Brushes.Black;
            ListBox listBox = (ListBox)(sender);
            LogEntry logEntry = (LogEntry)(listBox.Items[e.Index]);

            switch (logEntry.Level)
            {
                case Architecture.Components.Logging.LogLevel.Trace:
                    brush = Brushes.Gray;
                    break;
                case DomainWatcher.Architecture.Components.Logging.LogLevel.Info:
                    brush = Brushes.DeepSkyBlue;
                    break;
                case DomainWatcher.Architecture.Components.Logging.LogLevel.Warn:
                    brush = Brushes.Gold;
                    break;
                case DomainWatcher.Architecture.Components.Logging.LogLevel.Error:
                    brush = Brushes.OrangeRed;
                    break;
            }

            e.DrawBackground();
            e.Graphics.DrawString(logEntry.ToString(), e.Font, brush, e.Bounds, StringFormat.GenericDefault);
            e.DrawFocusRectangle();
        }

        #endregion

        #region IView<ILoggingPresenter,ILoggingView> Members

        public BindingList<LogEntry> LogEntries
        {
            get { return (BindingList<LogEntry>)this.listBox.DataSource; }
            set { this.listBox.DataSource = value; }
        }

        public void Initialize()
        {
            this.InitializeComponent();
        }

        #endregion
    }
}
