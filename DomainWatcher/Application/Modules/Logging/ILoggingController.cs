﻿namespace DomainWatcher.Application.Modules.Logging
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using DomainWatcher.Architecture.Contracts;

    public interface ILoggingController : IController<ILoggingView, ILoggingPresenter>
    {
    }
}
