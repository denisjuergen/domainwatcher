﻿namespace DomainWatcher.Application.Modules.Logging
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using DomainWatcher.Architecture.Components.Logging;

    public struct LogEntry
    {
        public string Message { get; set; }
        public LogLevel Level { get; set; }
        public DateTime Time { get; set; }

        public static LogEntry Create(string message, LogLevel level)
        {
            return new LogEntry { Message = message, Level = level, Time = DateTime.Now };
        }

        public override string ToString()
        {
            return string.Format("{0}: [{1}] {2}", this.Time, this.Level, this.Message);
        }
    }
}
