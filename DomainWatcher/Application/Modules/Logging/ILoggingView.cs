﻿namespace DomainWatcher.Application.Modules.Logging
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using DomainWatcher.Architecture.Contracts;
    using System.ComponentModel;

    public interface ILoggingView : IView<ILoggingPresenter, ILoggingView>
    {
        BindingList<LogEntry> LogEntries { get; set; }
    }
}
