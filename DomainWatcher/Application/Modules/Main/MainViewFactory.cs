﻿namespace DomainWatcher.Application.Modules.Main
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using DomainWatcher.Architecture.Components;
    using Microsoft.Practices.Unity;

    public class MainViewFactory : ViewFactory<IMainView, IMainPresenter>, IMainViewFactory
    {
    }
}
