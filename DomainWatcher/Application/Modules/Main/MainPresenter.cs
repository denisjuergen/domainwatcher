﻿namespace DomainWatcher.Application.Modules.Main
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using DomainWatcher.Architecture.Components;

    public class MainPresenter : Presenter<IMainView, IMainPresenter>, IMainPresenter
    {

    }
}
