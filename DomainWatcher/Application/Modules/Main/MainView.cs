﻿namespace DomainWatcher.Application.Modules.Main
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Data;
    using System.Drawing;
    using System.Linq;
    using System.Text;
    using System.Windows.Forms;
    using DomainWatcher.Architecture.Contracts;

    public partial class MainView : Form, IMainView
    {
        #region Constructors

        public MainView()
        {
        }

        #endregion

        #region IView<MainPresenter, MainView> Members

        public void Initialize()
        {
            this.InitializeComponent();
        }

        #endregion
    }
}
