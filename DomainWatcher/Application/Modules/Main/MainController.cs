﻿namespace DomainWatcher.Application.Modules.Main
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using DomainWatcher.Architecture.Components;

    public class MainController : Controller<IMainView, IMainPresenter>, IMainController
    {
        #region Constructors

        public MainController(IMainPresenter presenter, IMainViewFactory viewFactory)
            : base(presenter, viewFactory)
        {

        }

        #endregion
    }
}
