﻿namespace DomainWatcher.Application.Modules.Main
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using DomainWatcher.Architecture.Components.Modules;
    using Microsoft.Practices.Unity;
    using DomainWatcher.Architecture.Components.Logging;

    public sealed class MainModule : IModule
    {
        #region Constructors

        public MainModule(ILogger logger, IUnityContainer container)
        {
            this.logger = logger;
            this.container = container;
        }

        #endregion

        #region IModule Members

        public void Initialize()
        {
            // Main Module Registration
            this.container.RegisterType<IMainView, MainView>(new ContainerControlledLifetimeManager())
                          .RegisterType<IMainPresenter, MainPresenter>(new ContainerControlledLifetimeManager())
                          .RegisterType<IMainController, MainController>(new ContainerControlledLifetimeManager())
                          .RegisterType<IMainViewFactory, MainViewFactory>(new ContainerControlledLifetimeManager());

            this.logger.Log("MainModule Initialized", LogLevel.Debug);
        }

        #endregion

        #region Private Fields

        private readonly ILogger logger;
        private readonly IUnityContainer container;

        #endregion
    }
}
