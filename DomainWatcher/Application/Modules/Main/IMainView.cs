﻿namespace DomainWatcher.Application.Modules.Main
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using DomainWatcher.Architecture.Contracts;

    public interface IMainView : IView<IMainPresenter, IMainView>
    {
    }
}
