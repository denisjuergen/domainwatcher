﻿namespace DomainWatcher.Application
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Data;
    using System.Drawing;
    using System.Linq;
    using System.Text;
    using System.Windows.Forms;
    using Microsoft.Practices.Unity;
    using DomainWatcher.Architecture.Components.Regions;
    using DomainWatcher.Infrastructure;
    using DomainWatcher.Application.Events;
    using DomainWatcher.Architecture.Components.Events;

    public partial class ApplicationShell : Form
    {
        [Dependency]
        public IRegionManager RegionManager { get; set; }

        [Dependency]
        public EventLocator<ApplicationStarted> ApplicationStarted { get; set; }

        public ApplicationShell()
        {
            this.InitializeComponent();
        }

        private void ApplicationShell_Load(object sender, EventArgs e)
        {
            this.RegionManager.RegisterRegion(this.pnlSidebar, RegionNames.ShellSidebar);
            this.RegionManager.RegisterRegion(this.pnlContent, RegionNames.ShellContent);
            this.RegionManager.RegisterRegion(this.pnlLoggingWindow, RegionNames.ShellLoggingWindow);

            this.ApplicationStarted.Event.Publish(new ApplicationStarted.Payload(this));
        }
    }
}
