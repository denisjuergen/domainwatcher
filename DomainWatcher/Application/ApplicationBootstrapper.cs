﻿namespace DomainWatcher.Application
{
    using System;
    using System.Linq;
    using System.Windows.Forms;
    using DomainWatcher.Architecture.Unity;
    using Microsoft.Practices.Unity;
    using DomainWatcher.Architecture.Components.Modules;
    using DomainWatcher.Architecture.Extensions;
    using DomainWatcher.Architecture.Components;
    using DomainWatcher.Application.Modules.Logging;
    using DomainWatcher.Application.Modules;

    public class ApplicationBootstrapper : UnityBootstrapper
    {
        #region Private Properties

        private static ApplicationBootstrapper Current
        {
            get { return m_bootstrapper.Value; }
        }

        #endregion

        #region Application Entry Point

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        public static void Main(string[] args)
        {
            ApplicationBootstrapper.Current.Run();
        }

        #endregion

        #region Protected Methods

        protected override void ConfigureModuleCatalog(IModuleCatalog moduleCatalog)
        {
            base.ConfigureModuleCatalog(moduleCatalog);

            moduleCatalog.RegisterModule<LoggingModule>()
                         .RegisterModule<ShellConfigurationModule>();
        }

        protected override Form CreateShell()
        {
            return ServiceLocator.Current.GetInstance<ApplicationShell>();
        }

        #endregion

        #region Private Fields

        private readonly static Lazy<ApplicationBootstrapper> m_bootstrapper = new Lazy<ApplicationBootstrapper>(() => new ApplicationBootstrapper());

        #endregion
    }
}
