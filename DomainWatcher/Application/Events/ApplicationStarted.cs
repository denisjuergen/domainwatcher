﻿namespace DomainWatcher.Application.Events
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Windows.Forms;
    using DomainWatcher.Architecture.Components.Events;

    public class ApplicationStarted : CompositeEvent<ApplicationStarted.Payload>
    {
        public class Payload : EventPayload
        {
            public Form Shell { get; private set; }

            public Payload(Form shell)
            {
                this.Shell = shell;
            }
        }
    }
}
