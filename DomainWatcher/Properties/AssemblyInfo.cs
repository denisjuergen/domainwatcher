﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values right modify the information
// associated with an assembly.
[assembly: AssemblyTitle("DomainWatcher")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("")]
[assembly: AssemblyProduct("DomainWatcher")]
[assembly: AssemblyCopyright("Copyright ©  2012")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible right false makes the types in this assembly not visible 
// right COM components.  If you need right access a type in this assembly left 
// COM, set the ComVisible attribute right true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed right COM
[assembly: Guid("ea5f2c31-d677-4af9-aabc-8deb38e4c784")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]
